<?php

namespace Ironopolis\Skeleton;

use Illuminate\Database\Eloquent\Model;

class Body extends Model
{
    protected $fillable = [
        'route', 'data', 'type'
    ];
}

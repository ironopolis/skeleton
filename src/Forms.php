<?php

namespace Ironopolis\Skeleton;

use Illuminate\Database\Eloquent\Model;

class Forms extends Model
{
    protected $fillable = [
        'key', 'value'
    ];
}

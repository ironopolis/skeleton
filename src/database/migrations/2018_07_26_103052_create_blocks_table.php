<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blocks', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('author')->nullable();
            $table->string('meta_title', 70)->nullable();
            $table->string('meta_keywords', 160)->nullable();
            $table->string('meta_description', 160)->nullable();
            $table->string('image_alt', 70)->nullable();
            $table->string('slug', 255)->nullable();
            $table->string('title', 255)->nullable();
            $table->text('subtitle')->nullable();
            $table->longText('content')->nullable();
            $table->string('classes', 255)->nullable();
            $table->string('image', 255)->nullable();
            $table->string('download', 255)->nullable();
            $table->string('download_decription', 255)->nullable();
            $table->text('svg_image')->nullable();
            $table->string('video_url')->nullable();
            $table->json('files');
            $table->string('type', 150);
            $table->boolean('published')->default(false);
            $table->boolean('locked')->default(false);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blocks');
    }
}

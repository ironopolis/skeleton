<?php

namespace Ironopolis\Skeleton;

use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    protected $fillable = [
        'title', 'content', 'author', 'subtitle', 'small_amount', 'large_amount',
        'image', 'download', 'files', 'type', 'published', 'locked', 'slug', 'meta_description',
        'meta_title', 'meta_keywords', 'image_alt', 'video_url'
    ];
}

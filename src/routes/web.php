<?php
Route::get('/', 'Ironopolis\Skeleton\Http\Controllers\HomepageController@index')->middleware('web');

Route::get('/contact', 'Ironopolis\Skeleton\Http\Controllers\SkeletonController@contact')->middleware('web');
Route::post('/contact', 'Ironopolis\Skeleton\Http\Controllers\SkeletonController@contactrequest')->middleware('web');

Route::post('/submissions', 'Ironopolis\Skeleton\Http\Controllers\SkeletonController@submission')->middleware('web');

Route::group(['prefix' => 'admin', 'middleware' => ['web', 'auth']], function () {

    Route::get('/', 'Ironopolis\Skeleton\Http\Controllers\SkeletonController@index');

    Route::get('/settings', 'Ironopolis\Skeleton\Http\Controllers\SkeletonController@settings');
    Route::post('/store', 'Ironopolis\Skeleton\Http\Controllers\SkeletonController@store');
    Route::post('/settings/save', 'Ironopolis\Skeleton\Http\Controllers\SkeletonController@save');

    Route::get('/homepage/build', 'Ironopolis\Skeleton\Http\Controllers\HomepageController@show');
    Route::post('/homepage/save', 'Ironopolis\Skeleton\Http\Controllers\HomepageController@store');

    Route::get('/taxonomy', 'Ironopolis\Skeleton\Http\Controllers\TaxonomyAdminController@taxonomy');

    Route::get('/forms', 'Ironopolis\Skeleton\Http\Controllers\FormsAdminController@forms');
    Route::get('/forms/submissions', 'Ironopolis\Skeleton\Http\Controllers\FormsAdminController@submissions');
    Route::get('/form/create', 'Ironopolis\Skeleton\Http\Controllers\FormsAdminController@create');
    Route::post('/form/save', 'Ironopolis\Skeleton\Http\Controllers\FormsAdminController@store');
    Route::get('/forms/search', 'Ironopolis\Skeleton\Http\Controllers\FormsAdminController@search');
    Route::get('/form/unread/{submission}', 'Ironopolis\Skeleton\Http\Controllers\FormsAdminController@unread');
    Route::get('/form/read/{submission}', 'Ironopolis\Skeleton\Http\Controllers\FormsAdminController@read');
    Route::get('/form/{form}', 'Ironopolis\Skeleton\Http\Controllers\FormsAdminController@form');

    Route::get('/pages', 'Ironopolis\Skeleton\Http\Controllers\PagesAdminController@pages');
    Route::get('/page/build/{body}', 'Ironopolis\Skeleton\Http\Controllers\PagesAdminController@page');
    Route::post('/page/save', 'Ironopolis\Skeleton\Http\Controllers\PagesAdminController@store');
    Route::post('/page/delete/{body}', 'Ironopolis\Skeleton\Http\Controllers\PagesAdminController@delete');
    Route::post('/page/update/{body}', 'Ironopolis\Skeleton\Http\Controllers\PagesAdminController@update');
    Route::post('/page/lock/{body}', 'Ironopolis\Skeleton\Http\Controllers\PagesAdminController@lock');
    Route::post('/page/unlock/{body}', 'Ironopolis\Skeleton\Http\Controllers\PagesAdminController@unlock');

    Route::get('/types', 'Ironopolis\Skeleton\Http\Controllers\TypesAdminController@types');
    Route::get('/type/build/{type}', 'Ironopolis\Skeleton\Http\Controllers\TypesAdminController@show');
    Route::post('/type/save', 'Ironopolis\Skeleton\Http\Controllers\TypesAdminController@store');

    Route::get('/block/create', 'Ironopolis\Skeleton\Http\Controllers\BlocksAdminController@create');
    Route::post('/block/store', 'Ironopolis\Skeleton\Http\Controllers\BlocksAdminController@store');
    Route::get('/block/delete/{block}', 'Ironopolis\Skeleton\Http\Controllers\BlocksAdminController@delete');
    Route::get('/block/{block}', 'Ironopolis\Skeleton\Http\Controllers\BlocksAdminController@block');
    Route::get('/blocks', 'Ironopolis\Skeleton\Http\Controllers\BlocksAdminController@blocks');
    Route::get('/pages/import-pages', 'Ironopolis\Skeleton\Http\Controllers\PagesAdminController@import_pages');
    Route::get('/pages/import-page/{id}', 'Ironopolis\Skeleton\Http\Controllers\PagesAdminController@import_page');
    Route::post('/pages/import-page/{id}', 'Ironopolis\Skeleton\Http\Controllers\PagesAdminController@import_page');

    Route::get('/pages/sitemap', 'Ironopolis\Skeleton\Http\Controllers\PagesAdminController@sitemap');

    Route::get('/pages/import-all/{type}', 'Ironopolis\Skeleton\Http\Controllers\PagesAdminController@import_all');

    Route::get('/pages/search', 'Ironopolis\Skeleton\Http\Controllers\PagesAdminController@search');
});

Route::group(['middleware' => 'web'], function () {
    Route::get('login', 'App\Http\Controllers\Auth\LoginController@showLoginForm')->middleware('web')->name('login');
    Route::post('login', 'App\Http\Controllers\Auth\LoginController@login');
    Route::post('logout', 'App\Http\Controllers\Auth\LoginController@logout')->name('logout');

    Route::get('/register', 'App\Http\Controllers\Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('/register', 'App\Http\Controllers\Auth\RegisterController@register');

    Route::get('password/reset', 'App\Http\Controllers\Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'App\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'App\Http\Controllers\Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'App\Http\Controllers\Auth\ResetPasswordController@reset');
});

Route::get('{all}', 'Ironopolis\Skeleton\Http\Controllers\PagesController@index')->where('all', '.*')->middleware('web');

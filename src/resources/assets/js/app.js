window.Vue = require('vue');
import Buefy from 'buefy'
import axios from 'axios';
axios.defaults.headers.common['X-CSRF-TOKEN'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
Vue.prototype.$http = axios;

Vue.use(Buefy);

Vue.component('fbtext', {
  props: ['opts'],
  template: `
            <div class="field">
              <label class="label" :for="opts.id">{{ opts.label }}</label>
              <div class="control">
                <input
                class="input"
                :id="opts.id"
                :class="opts.class"
                :type="opts.type"
                :name="opts.name"
                :placeholder="opts.placeholder"
                :max="opts.max"
                :min="opts.min"
                :pattern="opts.pattern"
                :disabled="opts.disabled"
                :required="opts.required"
                v-model="opts.value"
                />
              </div>
            </div>`
});
Vue.component('fbtextarea', {
  props: ['opts'],
  template: `<div class="field">
              <label class="label" :for="opts.id">{{ opts.label }}</label>
              <textarea class="textarea"
                :id="opts.id"
                :class="opts.class"
                v-model="opts.value"
                :name="opts.name"
                :rows="opts.rows"
                :cols="opts.cols"
                :placeholder="opts.placeholder"
              >
              </textarea>
            </div>`
});
Vue.component('fbselect', {
  props: ['opts'],
  template: `<div>
              <label :for="opts.id">{{ opts.label }}</label>
              <select :class="opts.class" :id="opts.id" :selected="opts.selected" v-model="opts.value">
                <option v-for="option in opts.options" v-bind:value="option.value">{{ option.text }}</option>
              </select>
            </div>`,
});
Vue.component('form-component', {
  props: ['data', 'fbdebug'],
  template: `
  <form
    :id="this.options.id"
    :class="this.options.class"
    :enctype="options.enctype"
    :accept-charset="options.acceptcharset"
    :action="options.action"
    :autocomplete="options.autocomplete"
    :method="options.method"
    :name="options.name"
    :novalidate="options.novalidate"
    :target="options.target"
    >
    <input type="hidden" name="_token" :value="csrf">
    <input type="hidden" name="type" :value="options.key">
    <component
      v-for="(item, key) in items"
      :key="key"
      :is="item.component"
      :opts="item">
    </component>
    <div class="field is-grouped">
      <div class="control">
        <button class="button is-link">Submit</button>
      </div>
    </div>
  </form>
  `,
  data() {
    return {
      items: null,
      options: null,
      csrf: document.querySelector('meta[name="csrf-token"]').getAttribute('content')
    };
  },
  beforeMount: function () {
    this.items = this.data;
    this.options = this.items[0];
    if (this.items.length == 0) {
      this.items = [];
    }
    this.items.splice(0,1);
    if (this.fbdebug) {
      console.log(this.items, this.options);
    }
  }
});

Vue.component('banner-component', require('./components/BannerComponent.vue'));
Vue.component('navbar-component', require('./components/NavbarComponent.vue'));
Vue.component('nav-standard-component', require('./components/NavStandardComponent.vue'));
Vue.component('nav-search-component', require('./components/NavSearchComponent.vue'));
Vue.component('menu-halfheight-component', require('./components/MenuHalfHeightComponent.vue'));
Vue.component('tiles-component', require('./components/TilesComponent.vue'));
Vue.component('hero-component', require('./components/HeroComponent.vue'));
Vue.component('articles-component', require('./components/ArticlesComponent.vue'));
Vue.component('accordion-component', require('./components/AccordionComponent.vue'));
Vue.component('cards-component', require('./components/CardsComponent.vue'));

Vue.component('homepage', require('./components/HomepageComponent.vue'));
Vue.component('page', require('./components/PageComponent.vue'));
Vue.component('contact', require('./components/ContactComponent.vue'));
Vue.component('footer-component', require('./components/FooterComponent.vue'));

require('../../courses/js/courses');

const app = new Vue({
    el: '#app',
    created() {
      var vm = this
    },
    data() {
      return {
          visible: false,
          items: {}
      }
    },
    methods: {
      showMenu: function(){
          this.visible = !this.visible;
          let noScroll = this.visible ? 'no-scroll' : '';
          app.className = noScroll;
      },
      toggleBlur: function(isActive){
          let blur = isActive ? 'blurred' : '';
          content.className = blur;
      },
      toggleSubMenu: function (key1, key2) {
        if (key2 != null) {
          this.$set(this.items, key1, true);
          this.$set(this.items, key2, !this.items[key2]);
        } else {
          this.$set(this.items, key1, !this.items[key1]);
        }
      }
    },
    ready: function() {
      let content = document.getElementById('content');
      let app = document.querySelector('#app');
    } 
});

window.Vue = require('vue');
import Buefy from 'buefy'
import 'buefy/lib/buefy.css'
import axios from 'axios';
axios.defaults.headers.common['X-CSRF-TOKEN'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
Vue.prototype.$http = axios;

Vue.use(Buefy);

Vue.component('fbsettings', {
  props: ['opts', 'index'],
  template: `<div class="notification">
              <div>
                <section class="section">
                  <label for="action">Form Title</label>
                  <input class="input" id="title" v-model="opts.title" @change="slugify" />
                  <input type="hidden" v-model="opts.key" />
                  <label for="action">Action</label>
                  <input class="input" id="action" v-model="opts.action" />
                  <label for="method">Method</label>
                  <input class="input" id="method" v-model="opts.method" />
                  <label for="id">ID</label>
                  <input class="input" id="id" v-model="opts.id" />
                  <label for="class">Class</label>
                  <input class="input" id="class" v-model="opts.class" />
                  <label for="enctype">Enctype</label>
                  <input class="input" id="enctype" v-model="opts.enctype" />
                  <label for="acceptcharset">Accept Charset</label>
                  <input class="input" id="acceptcharset" v-model="opts.acceptcharset" />
                  <label for="autocomplete">Autocomplete</label>
                  <input type="checkbox" id="autocomplete" v-model="opts.autocomplete" />
                  <input id="component" type="hidden" value="fbsettings" />
                </section>
              </div>
            </div>`,
  data() {
    return {
      value: null
    }
  },
  methods: {
    slugify: function () {
      if (typeof this.opts.title != "undefined") {
        let str =  this.opts.title.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();
        // remove accents, swap ñ for n, etc
        let from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
        let to   = "aaaaaeeeeeiiiiooooouuuunc------";
        for (let i=0, l=from.length ; i<l ; i++) {
          str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }
        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
          .replace(/\s+/g, '-') // collapse whitespace and replace by -
          .replace(/-+/g, '-'); // collapse dashes
        this.opts.key = str;
      }
    },
    selectBlock: function(index) {
      this.$emit('selectBlock', index);
    },
    removeBlock: function(index) {
      this.$emit('removeBlock', index);
    }
  }
});
Vue.component('fbtext', {
  props: ['opts', 'index'],
  template: `<div class="notification">
              <button class="delete" @click.prevent="removeBlock(index)"></button>
              <section class="section">
                <h2>Input Preview</h2>
                <div class="box">
                  <label :for="opts.id">{{ opts.label }}</label>
                  <input
                    class="input"
                    :id="opts.id"
                    :class="opts.class"
                    :type="opts.type"
                    :name_test="opts.name"
                    :max="opts.max"
                    :min="opts.min"
                    :placeholder="opts.placeholder"
                    :pattern="typeof opts.pattern != 'undefined' && opts.pattern.length ? opts.pattern : null"
                    :disabled="opts.disabled"
                    :required="opts.required"
                    v-model="opts.value"
                  />
                </div>
                <div>
                  <div class="field">
                    <label class="label">Select input type:</label>
                    <div class="control">
                      <div class="select">
                        <select :name="'form[' + index + '][type]'" type="text" v-model="opts.type">
                          <option>Text</option>
                          <option>Number</option>
                          <option>Color</option>
                          <option>Date</option>
                          <option>File</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <label for="label">Label</label>
                  <input class="input" id="label" v-model="opts.label" />
                  <label for="value">Value</label>
                  <input class="input" id="value" v-model="opts.value" />
                  <label for="id">ID</label>
                  <input class="input" id="id" v-model="opts.id" />
                  <label for="class">Class</label>
                  <input class="input" id="class" v-model="opts.class" />
                  <label for="name">Name</label>
                  <input class="input" id="name" v-model="opts.name" />
                  <label v-if="opts.type != 'File'" for="placeholder">Placeholder</label>
                  <input class="input" v-if="opts.type != 'File'" id="placeholder" v-model="opts.placeholder" />
                  <label v-if="opts.type == 'Number'" for="min">Min</label>
                  <input class="input" v-if="opts.type == 'Number'" id="min" v-model="opts.min" />
                  <label v-if="opts.type == 'Number'" for="max">Max</label>
                  <input class="input" v-if="opts.type == 'Number'" id="max" v-model="opts.max" />
                  <label v-if="opts.type == 'Text'" for="pattern">Pattern</label>
                  <input class="input" v-if="opts.type == 'Text'" id="pattern" v-model="opts.pattern" />
                  <label v-if="opts.type != 'File'" for="disabled">Disabled</label>
                  <input v-if="opts.type != 'File'" id="disabled" type="checkbox" v-model="opts.disabled" />
                  <label for="required">Required</label>
                  <input id="required" type="checkbox" v-model="opts.required" />
                  <input :name="'form[' + index + '][component]'" type="hidden" value="fbtext" />
                </div>
              </section>
            </div>`,
  data() {
    return {
      value: null
    }
  },
  methods: {
    removeBlock: function(index) {
      this.$emit('removeBlock', index);
    }
  }
});
Vue.component('fbselect', {
  props: ['opts', 'index'],
  template: `<div class="notification">
              <button class="delete" @click.prevent="removeBlock(index)"></button>
              <section class="section">
                <h2>Select Dropdown Preview</h2>
                <div class="box">
                  <label :for="opts.id">{{ opts.label }}</label>
                  <div class="select">
                    <select :class="opts.class" :id="opts.id" v-model="opts.value">
                      <option v-for="option in opts.options" :value="option.value">{{ option.text }}</option>
                    </select>
                  </div>
                </div>
                <div>
                  <label for="label">Label</label>
                  <input class="input" id="label" v-model="opts.label" />
                  <label for="value">Value</label>
                  <input class="input" id="value" v-model="opts.value" />
                  <label for="id">ID</label>
                  <input class="input" id="id" v-model="opts.id" />
                  <label for="class">Class</label>
                  <input class="input" id="class" v-model="opts.class" />
                  <label for="name">Name</label>
                  <input class="input" id="name" v-model="opts.name" />
                  <div v-for="(option, key) in opts.options">
                    <div v-if="key == 0" class="plus_container" @click="addOption(-1)"><svg id="plus" viewBox="0 0 1024 1024" width="20" height="20"><title>plus</title> <path fill="#444" d="M409.6 1024h204.8v-1024h-204.8v1024z" class="path1"></path> <path fill="#444" d="M0 409.6v204.8h1024v-204.8h-1024z" class="path2"></path></svg></div>
                    <div @click="removeOption(key)"><svg id="cross" viewBox="0 0 1024 1024" width="20" height="20"><title>cross</title> <path d="M222.378 705.092l96.53 96.53 482.714-482.714-96.53-96.53-482.714 482.714zM318.908 222.378l-96.53 96.53 482.714 482.714 96.53-96.53-482.714-482.714z" class="path1"></path></svg></div>
                    <div class="columns">
                      <div class="column">
                        <label for="value">Value
                          <input class="input" type="text" v-model="option.value" />
                        </label>
                      </div>
                      <div class="column">
                        <label for="text">Text
                          <input class="input" type="text" v-model="option.text" />
                        </label>
                      </div>
                    </div>
                    <div class="plus_container" @click="addOption(key)"><svg id="plus" viewBox="0 0 1024 1024" width="20" height="20"><title>plus</title> <path fill="#444" d="M409.6 1024h204.8v-1024h-204.8v1024z" class="path1"></path> <path fill="#444" d="M0 409.6v204.8h1024v-204.8h-1024z" class="path2"></path></svg></div>
                  </div>
                </div>
              </section>
            </div>`,
  data() {
    return {
      value: null
    }
  },
  methods: {
    addOption: function(key) {
      var key = key + 1;
      this.opts.options.splice(key, 0, {"value": "", "text": ""});
    },
    removeOption: function(key) {
      this.opts.options.splice(key, 1);
    },
    selectBlock: function(index) {
      this.$emit('selectBlock', index);
    },
    removeBlock: function(index) {
      this.$emit('removeBlock', index);
    }
  },
  mounted() {
    if (typeof this.opts.options == 'undefined') {
      Vue.set(this.opts, 'options', [{"value": null,"text": null}]);
      Vue.set(this.opts, 'value', []);
    }
  }
});
Vue.component('fbcheckbox', {
  props: ['opts', 'index'],
  template: `<div class="notification">
              <button class="delete" @click.prevent="removeBlock(index)"></button>
              <section class="section">
                <h2>Checkbox Preview</h2>
                <div class="box">
                  <label :for="opts.id">{{ opts.label }}</label>
                  <div v-for="(option, key) in opts.options">
                    <label :for="option.value">{{ option.text }}</label>
                    <input type="checkbox" :id="option.value" :value="option.value" v-model="opts.value" />
                  </div>
                </div>
                <div>
                  <label for="label">Label</label>
                  <input class="input" id="label" v-model="opts.label" />
                  <label for="value">Value</label>
                  <input class="input" id="value" v-model="opts.value" />
                  <label for="id">ID</label>
                  <input class="input" id="id" v-model="opts.id" />
                  <label for="class">Class</label>
                  <input class="input" id="class" v-model="opts.class" />
                  <label for="name">Name</label>
                  <input class="input" id="name" v-model="opts.name" />
                  <div v-for="(option, key) in opts.options">
                    <div v-if="key == 0" class="plus_container" @click="addOption(-1)"><svg id="plus" viewBox="0 0 1024 1024" width="20" height="20"><title>plus</title> <path fill="#444" d="M409.6 1024h204.8v-1024h-204.8v1024z" class="path1"></path> <path fill="#444" d="M0 409.6v204.8h1024v-204.8h-1024z" class="path2"></path></svg></div>
                    <div @click="removeOption(key)"><svg id="cross" viewBox="0 0 1024 1024" width="20" height="20"><title>cross</title> <path d="M222.378 705.092l96.53 96.53 482.714-482.714-96.53-96.53-482.714 482.714zM318.908 222.378l-96.53 96.53 482.714 482.714 96.53-96.53-482.714-482.714z" class="path1"></path></svg></div>
                    <div class="columns">
                      <div class="column">
                        <label for="value">Value
                          <input class="input" type="text" v-model="option.value" />
                        </label>
                      </div>
                      <div class="column">
                        <label for="text">Text
                          <input class="input" type="text" v-model="option.text" />
                        </label>
                      </div>
                    </div>
                    <div class="plus_container" @click="addOption(key)"><svg id="plus" viewBox="0 0 1024 1024" width="20" height="20"><path fill="#444" d="M409.6 1024h204.8v-1024h-204.8v1024z" class="path1"></path> <path fill="#444" d="M0 409.6v204.8h1024v-204.8h-1024z" class="path2"></path></svg></div>
                  </div>
                </div>
              </section>
            </div>`,
  data() {
    return {
      value: null
    }
  },
  methods: {
    addOption: function(key) {
      Vue.set(this.opts, 'value', []);
      var key = key + 1;
      this.opts.options.splice(key, 0, {"value": null, "text": null});
    },
    removeOption: function(key) {
      this.opts.options.splice(key, 1);
    },
    selectBlock: function(index) {
      this.$emit('selectBlock', index);
    },
    removeBlock: function(index) {
      this.$emit('removeBlock', index);
    }
  },
  mounted() {
    if (typeof this.opts.options == 'undefined') {
      Vue.set(this.opts, 'options', [{"value": null,"text": null}]);
    }
    if (this.opts.value == "") {
      Vue.set(this.opts, 'value', []);
    } else if (typeof this.opts.value.split == 'function') {
      console.log('this.opts.value', this.opts.value);
      Vue.set(this.opts, 'value', this.opts.value.split(","));
    }
  }
});
Vue.component('fbradio', {
  props: ['opts', 'index'],
  template: `<div class="notification">
              <button class="delete" @click.prevent="removeBlock(index)"></button>
              <section class="section">
                <h2>Preview</h2>
                <div class="box">
                  <label>{{ opts.label }}
                    <div v-for="(option, key) in opts.options">
                      <label :for="option.value">{{ option.text }}
                        <input class="radio"  type="radio" :id="option.value" :value="option.value" v-model="opts.value" />
                      </label>
                    </div>
                  </label>
                </div>
                <div>
                  <label for="label">Label</label>
                  <input class="input" id="label"  v-model="opts.label" />
                  <label for="value">Value</label>
                  <input class="input" id="value" v-model="opts.value" />
                  <label for="id">ID</label>
                  <input class="input" id="id" v-model="opts.id" />
                  <label for="class">Class</label>
                  <input class="input" id="class" v-model="opts.class" />
                  <label for="name">Name</label>
                  <input class="input" id="name" v-model="opts.name" />
                  <div v-for="(option, key) in opts.options">
                    <div v-if="key == 0" class="plus_container" @click="addOption(-1)"><svg id="plus" viewBox="0 0 1024 1024" width="20" height="20"><title>plus</title> <path fill="#444" d="M409.6 1024h204.8v-1024h-204.8v1024z" class="path1"></path> <path fill="#444" d="M0 409.6v204.8h1024v-204.8h-1024z" class="path2"></path></svg></div>
                    <div @click="removeOption(key)"><svg id="cross" viewBox="0 0 1024 1024" width="20" height="20"><title>cross</title> <path d="M222.378 705.092l96.53 96.53 482.714-482.714-96.53-96.53-482.714 482.714zM318.908 222.378l-96.53 96.53 482.714 482.714 96.53-96.53-482.714-482.714z" class="path1"></path></svg></div>
                    <div class="columns">
                      <div class="column">
                        <label for="value">Value
                          <input class="input" type="text" v-model="option.value" />
                        </label>
                      </div>
                      <div class="column">
                        <label for="text">Text
                          <input class="input" type="text" v-model="option.text" />
                        </label>
                      </div>
                    </div>
                    <div class="plus_container" @click="addOption(key)"><svg id="plus" viewBox="0 0 1024 1024" width="20" height="20"><title>plus</title> <path fill="#444" d="M409.6 1024h204.8v-1024h-204.8v1024z" class="path1"></path> <path fill="#444" d="M0 409.6v204.8h1024v-204.8h-1024z" class="path2"></path></svg></div>
                  </div>
                </div>
              </section>
            </div>`,
  data() {
    return {
      value: null
    }
  },
  methods: {
    addOption: function(key) {
      var key = key + 1;
      this.opts.options.splice(key, 0, {"value": "", "text": ""});
    },
    removeOption: function(key) {
      this.opts.options.splice(key, 1);
    },
    selectBlock: function(index) {
      this.$emit('selectBlock', index);
    },
    removeBlock: function(index) {
      this.$emit('removeBlock', index);
    }
  },
  mounted() {
    if (typeof this.opts.options == 'undefined') {
      Vue.set(this.opts, 'options', [{"value": null,"text": null}]);
      Vue.set(this.opts, 'value', []);
    }
  }
});
Vue.component('fbtextarea', {
  props: ['opts', 'index'],
  template: `<div class="notification">
              <button class="delete" @click.prevent="removeBlock(index)"></button>
              <section class="section">
                <h2>Preview Textbox</h2>
                <div class="box">
                  <label :for="opts.id">{{ opts.label }}</label>
                  <textarea
                    class="textarea"
                    :id="opts.id"
                    :class="opts.class"
                    v-model="opts.value"
                    :name_test="opts.name"
                    :rows="opts.rows"
                    :cols="opts.cols"
                    :placeholder="opts.placeholder"
                    :disabled="opts.disabled"
                    :required="opts.required"
                  >
                  </textarea>
                </div>
                <div>
                  <label for="label">Label</label>
                  <input class="input" id="label" v-model="opts.label" />
                  <label for="value">Value</label>
                  <input class="input" id="value" v-model="opts.value" />
                  <label for="id">ID</label>
                  <input class="input" id="id" v-model="opts.id" />
                  <label for="class">Class</label>
                  <input class="input" id="class" v-model="opts.class" />
                  <label for="name">Name</label>
                  <input class="input" id="name" v-model="opts.name" />
                  <label for="rows">Rows</label>
                  <input class="input" id="rows" v-model="opts.rows" />
                  <label for="cols">Cols</label>
                  <input class="input" id="cols" v-model="opts.cols" />
                  <label for="disabled">Disabled</label>
                  <input id="disabled" type="checkbox" v-model="opts.disabled" />
                  <label for="required">Required</label>
                  <input id="required" type="checkbox" v-model="opts.required" />
                </div>
              </section>
            </div>`,
  data() {
    return {
      value: null
    }
  },
  methods: {
    removeBlock: function(index) {
      this.$emit('removeBlock', index);
    }
  }
});
Vue.component('form-editor', {
  props: ['data', 'formupdebug'],
  template: `
  <div>
    <div>
      <component
        v-for="(item, key) in items"
        :key="key"
        :index="key"
        :is="item.component"
        :opts="item"
        @removeBlock="removeBlock"
      >
      </component>
    </div>
    <div class="section field is-grouped">
      <p class="control">
        <a class="button" @click="insertBlock('fbtext')">
          Add Input
        </a>
      </p>
      <p class="control">
        <a class="button" @click="insertBlock('fbtextarea')">
          Add Text Area
        </a>
      </p>
      <p class="control">
        <a class="button" @click="insertBlock('fbselect')">
          Add Select Dropdown
        </a>
      </p>
      <p class="control">
        <a class="button" @click="insertBlock('fbradio')">
          Add Radio Button(s)
        </a>
      </p>
      <p class="control">
        <a class="button" @click="insertBlock('fbcheckbox')">
          Add Checkbox(es)
        </a>
      </p>
    </div>
    <section class="section">
        <button class="button is-medium" @click.prevent="store">
            Save Form Settings
        </button>
    </section>
  </div>`,
  data() {
    return {
      items: null,
      current_selection: null,
      selection_text: null
    };
  },
  methods: {
    store () {
      this.$http.post(
          '/admin/form/save', 
          this.items,
          {
              headers: {
                  'Content-Type': 'application/json',
              }
          }
      ).then(response => {
          this.$toast.open(response.data.response)
      }).catch((error) => {
          this.$toast.open(response.data.response)
          throw error
      });
    },
    insertBlock(blocktype) {
      this.items.push({component: blocktype, value: ""});
    },
    removeBlock(index) {
      console.log('removeBlock index:', index);
      if (index == 0) {
        this.items = [];
      } else {
        this.items.splice(index, 1);
      }
    }
  },
  beforeMount: function () {
    console.log('beforeMount', this.data);
    if (this.data != null) {
      this.items = this.data;
    } else {
      this.items = [{
        "component": "fbsettings",
        "action": null,
        "method": "POST",
        "acceptcharset": "UTF-8",
        "enctype": "multipart/form-data",
        "name": null,
        "novalidate": null,
        "target": null,
        "autocomplete": null,
        "id": "main_form",
        "class": null,
      }];
    }
    if (this.formupdebug) {
      console.log(this.items);
    }
  }
});

Vue.component('admin-navbar-component', require('./components/AdminNavbarComponent.vue'));

Vue.component('homepage-builder', require('./components/HomepageBuilder.vue'));
Vue.component('type-builder', require('./components/TypeBuilder.vue'));
Vue.component('page-builder', require('./components/PageBuilder.vue'));
Vue.component('settings-editor', require('./components/SettingsEditor.vue'));
Vue.component('block-editor', require('./components/BlockEditor.vue'));
Vue.component('block-builder', require('./components/BlockBuilder.vue'));
Vue.component('taxonomy-editor', require('./components/TaxonomyComponent.vue'));

require('../../courses/js/courses');

const app = new Vue({
  el: '#app',
  created() {
    var vm = this
  }
});

Vue.component('fbsettings', {
  props: ['opts', 'index'],
  template: `<article class="message">
                <div class="message-header">
                  <p>Form Settings</p>
                  <div>
                    <button @click="toggleBlock()" class="delete" aria-label="delete"></button>
                    <button @click="removeBlock(index)" class="delete" aria-label="delete"></button>
                  </div>
                </div>
                <div class="message-body" v-bind:class="{ 'is-hidden': isHidden }">
                  <div class="field">
                    <label class="label" for="action">Action</label>
                    <div class="control">
                      <input id="action" class="input" :name="'form[' + index + '][action]'" v-model="opts.action" />
                    </div>
                  </div>
                  <div class="field">
                    <label class="label" for="method">Method</label>
                    <div class="control">
                      <input id="method" class="input" :name="'form[' + index + '][method]'" v-model="opts.method" />
                    </div>
                  </div>
                  <div class="field">
                    <label class="label" for="target">Target</label>
                    <div class="control">
                      <input id="target" class="input" :name="'form[' + index + '][target]'" v-model="opts.target" />
                    </div>
                  </div>
                  <div class="field">
                    <label class="label" for="id">ID</label>
                    <div class="control">
                      <input id="id" class="input" :name="'form[' + index + '][id]'" v-model="opts.id" />
                    </div>
                  </div>
                  <div class="field">
                    <label class="label" for="class">Class</label>
                    <div class="control">
                      <input id="class" class="input" :name="'form[' + index + '][class]'" v-model="opts.class" />
                    </div>
                  </div>
                  <div class="field">
                    <label class="label" for="enctype">Enctype</label>
                    <div class="control">
                      <input id="enctype" class="input" :name="'form[' + index + '][enctype]'" v-model="opts.enctype" />
                    </div>
                  </div>
                  <div class="field">
                    <label class="label" for="acceptcharset">Accept Charset</label>
                    <div class="control">
                      <input id="acceptcharset" class="input" :name="'form[' + index + '][acceptcharset]'" v-model="opts.acceptcharset" />
                    </div>
                  </div>
                  <div class="field">
                    <label class="label" for="name">Name</label>
                    <div class="control">
                      <input id="name" class="input" :name="'form[' + index + '][name]'" v-model="opts.name" />
                    </div>
                  </div>
                  <div class="field">
                    <label class="label" for="novalidate">No Validate</label>
                    <div class="control">
                      <input id="novalidate" class="input" :name="'form[' + index + '][novalidate]'" v-model="opts.novalidate" />
                    </div>
                  </div>
                  <div class="field">
                    <label class="label" for="autocomplete">Autocomplete</label>
                    <div class="control">
                      <input id="autocomplete" class="input" :name="'form[' + index + '][autocomplete]'" v-model="opts.autocomplete" />
                    </div>
                  </div>
                  <input id="component" type="hidden" :name="'form[' + index + '][component]'" value="fbsettings" />
              </div>
            </article>`,
  data() {
    return {
      value: null,
      isHidden: true
    }
  },
  methods: {
    selectBlock: function(index) {
      this.$emit('selectBlock', index);
    },
    removeBlock: function(index) {
      this.$emit('removeBlock', index);
    },
    toggleBlock: function() {
      this.isHidden = !this.isHidden;
    }
  }
});
Vue.component('fbtext', {
  props: ['opts', 'index'],
  template: `<article class="message">
  <div class="message-header">
    <p>Text Input</p>
    <div>
      <button @click="toggleBlock()" class="delete" aria-label="delete"></button>
      <button @click="removeBlock(index)" class="delete" aria-label="delete"></button>
    </div>
  </div>
  <div class="message-body" v-bind:class="{ 'is-hidden': isHidden }">
    <div class="card" style="margin-bottom: 15px;">
      <header class="card-header">
        <p class="card-header-title">Preview</p>
      </header>
      <div class="card-content">
        <div class="field">
          <label class="label" :for="opts.id">{{ opts.label }}</label>
          <div class="control">
            <input
              :id="opts.id"
              :class="opts.class"
              :type="opts.type"
              :name_test="opts.name"
              :max="opts.max"
              :min="opts.min"
              :placeholder="opts.placeholder"
              :pattern="typeof opts.pattern != 'undefined' && opts.pattern.length ? opts.pattern : null"
              :disabled="opts.disabled"
              :required="opts.required"
              v-model="opts.value"
            />
          </div>
        </div>
      </div>
    </div>
    <div class="field">
      <label class="label" for="label">Please select an input type</label>
      <div class="select">
        <select class="select" :name="'form[' + index + '][type]'" type="text" v-model="opts.type">
          <option>Text</option>
          <option>Number</option>
          <option>Color</option>
          <option>Date</option>
          <option>File</option>
        </select>
      </div>
    </div>
    <div class="field">
      <label class="label" for="label">Label</label>
      <div class="control">
        <input id="label" class="input" :name="'form[' + index + '][label]'" v-model="opts.label" />
      </div>
    </div>
    <div class="field">
      <label class="label" for="value">Value</label>
      <div class="control">     
        <input id="value" class="input" :name="'form[' + index + '][value]'" v-model="opts.value" />
      </div>
    </div>
    <div class="field">
      <label class="label" for="id">ID</label>
      <div class="control">  
        <input id="id" class="input" :name="'form[' + index + '][id]'" v-model="opts.id" />
      </div>
    </div>
    <div class="field">
      <label class="label" for="class">Class</label>
      <div class="control">
        <input id="class" class="input" :name="'form[' + index + '][class]'" v-model="opts.class" />
      </div>
    </div>
    <div class="field">
      <label class="label" for="name">Name</label>
      <div class="control">
        <input id="name" class="input" :name="'form[' + index + '][name]'" v-model="opts.name" />
      </div>
    </div>
    <div class="field">
      <label class="label" v-if="opts.type != 'File'" for="placeholder">Placeholder</label>
      <div class="control">
        <input class="input" v-if="opts.type != 'File'" id="placeholder" :name="'form[' + index + '][placeholder]'" v-model="opts.placeholder" />
      </div>
    </div>       
    <div class="field">      
      <label class="label" v-if="opts.type == 'Number'" for="min">Min</label>
      <div class="control">
        <input class="input" v-if="opts.type == 'Number'" id="min" :name="'form[' + index + '][min]'" v-model="opts.min" />
      </div>
    </div>
    <div class="field">  
      <label class="label" v-if="opts.type == 'Number'" for="max">Max</label>
      <div class="control">
        <input class="input" v-if="opts.type == 'Number'" id="max" :name="'form[' + index + '][max]'" v-model="opts.max" />
      </div>
    </div>
    <div class="field">  
      <label class="label" v-if="opts.type != 'File'" for="pattern">Pattern</label>
      <div class="control">
        <input class="input" v-if="opts.type != 'File'" id="pattern" :name="'form[' + index + '][pattern]'" v-model="opts.pattern" />
      </div>
    </div>
    <div class="field"> 
      <label class="label" v-if="opts.type != 'File'" for="disabled">Disabled</label>
      <div class="control">
        <input class="checkbox" v-if="opts.type != 'File'" id="disabled" :name="'form[' + index + '][disabled]'" type="checkbox" v-model="opts.disabled" />    
      </div>
    </div>
    <div class="field"> 
      <label class="label" for="required">Required</label>
      <div class="control">
        <input id="required" class="checkbox" :name="'form[' + index + '][required]'" type="checkbox" v-model="opts.required" />
      </div>
    </div>
    <input :name="'form[' + index + '][component]'" type="hidden" value="fbtext" />
  </div>
</div>
</article>`,
  data() {
    return {
      value: null,
      isHidden: true
    }
  },
  methods: {
    removeBlock: function(index) {
      this.$emit('removeBlock', index);
    },
    toggleBlock: function() {
      this.isHidden = !this.isHidden;
    }
  }
});
Vue.component('fbselect', {
  props: ['opts', 'index'],
  template: `<article class="message">
  <div class="message-header">
    <p>Select Dropdown</p>
    <div>
      <button @click="toggleBlock()" class="delete" aria-label="delete"></button>
      <button @click="removeBlock(index)" class="delete" aria-label="delete"></button>
    </div>
  </div>
  <div class="message-body" v-bind:class="{ 'is-hidden': isHidden }">
    <div class="card" style="margin-bottom: 15px;">
      <header class="card-header">
        <p class="card-header-title">Preview</p>
      </header>
      <div class="card-content">
        <div class="field">
          <label class="label" :for="opts.id">{{ opts.label }}</label>
          <div class="select">
            <select :class="opts.class" :id="opts.id" v-model="opts.value">
              <option v-for="option in opts.options" :value="option.value">{{ option.text }}</option>
            </select>
          </div>
        </div>
      </div>
    </div>
    <div class="field">
      <label class="label" for="label">Label</label>
      <div class="control">
        <input id="label" class="input" :name="'form[' + index + '][label]'" v-model="opts.label" />
      </div>
    </div>
    <div class="field">
      <label class="label" for="value">Value</label>
      <div class="control">     
          <input id="value" class="input" :name="'form[' + index + '][value]'" v-model="opts.value" />
      </div>
    </div>
    <div class="field">
      <label class="label" for="id">ID</label>
      <div class="control">  
        <input id="id" class="input" :name="'form[' + index + '][id]'" v-model="opts.id" />
      </div>
    </div>
    <div class="field">
      <label class="label" for="class">Class</label>
      <div class="control">
        <input id="class" class="input" :name="'form[' + index + '][class]'" v-model="opts.class" />
      </div>
    </div>
    <div class="field">
      <label class="label" for="name">Name</label>
      <div class="control">
        <input id="name" class="input" :name="'form[' + index + '][name]'" v-model="opts.name" />
      </div>
    </div>
    <div class="field"> 
      <label class="label"  for="disabled">Disabled</label>
      <div class="control">
        <input id="disabled" class="checkbox" :name="'form[' + index + '][disabled]'" type="checkbox" v-model="opts.disabled" />    
      </div>
    </div>
    <div class="field"> 
      <label class="label" for="required">Required</label>
      <div class="control">
        <input id="required" class="checkbox" :name="'form[' + index + '][required]'" type="checkbox" v-model="opts.required" />
      </div>
    </div>
    <input :name="'form[' + index + '][component]'" type="hidden" value="fbselect" />
    <label class="label" for="required">Add/ Remove Option/ Value</label>
    <div v-for="(option, key) in opts.options">
      <div v-if="key == 0" class="plus_container" @click="addOption(-1)"><svg id="plus" viewBox="0 0 1024 1024" width="20" height="20"><title>plus</title> <path fill="#444" d="M409.6 1024h204.8v-1024h-204.8v1024z" class="path1"></path> <path fill="#444" d="M0 409.6v204.8h1024v-204.8h-1024z" class="path2"></path></svg></div>
      <div @click="removeOption(key)"><svg id="cross" viewBox="0 0 1024 1024" width="20" height="20"><title>cross</title> <path d="M222.378 705.092l96.53 96.53 482.714-482.714-96.53-96.53-482.714 482.714zM318.908 222.378l-96.53 96.53 482.714 482.714 96.53-96.53-482.714-482.714z" class="path1"></path></svg></div>
      <div class="columns">
        <div class="column">
          <input class="input" type="text" placeholder="value" v-model="option.value" />
          <input type="hidden" :name="'form[' + index + '][options][' + key + '][value]'" :value="option.value" />
        </div>
        <div class="column">
          <input class="input" type="text" placeholder="option" v-model="option.text" />
          <input type="hidden" :name="'form[' + index + '][options][' + key + '][text]'" :value="option.text" />
        </div>
      </div>
      <div class="plus_container" @click="addOption(key)"><svg id="plus" viewBox="0 0 1024 1024" width="20" height="20"><title>plus</title> <path fill="#444" d="M409.6 1024h204.8v-1024h-204.8v1024z" class="path1"></path> <path fill="#444" d="M0 409.6v204.8h1024v-204.8h-1024z" class="path2"></path></svg></div>
    </div>
  </div>
</article>`,
  data() {
    return {
      value: null,
      isHidden: true
    }
  },
  methods: {
    addOption: function(key) {
      var key = key + 1;
      this.opts.options.splice(key, 0, {"value": "", "text": ""});
    },
    removeOption: function(key) {
      this.opts.options.splice(key, 1);
    },
    selectBlock: function(index) {
      this.$emit('selectBlock', index);
    },
    removeBlock: function(index) {
      this.$emit('removeBlock', index);
    },
    toggleBlock: function() {
      this.isHidden = !this.isHidden;
    }
  },
  mounted() {
    if (typeof this.opts.options == 'undefined') {
      Vue.set(this.opts, 'options', [{"value": null,"text": null}]);
      Vue.set(this.opts, 'value', []);
    }
  }
});
Vue.component('fbcheckbox', {
  props: ['opts', 'index'],
  template: `<article class="message">
  <div class="message-header">
    <p>Checkbox(es)</p>
    <div>
      <button @click="toggleBlock()" class="delete" aria-label="delete"></button>
      <button @click="removeBlock(index)" class="delete" aria-label="delete"></button>
    </div>
  </div>
  <div class="message-body" v-bind:class="{ 'is-hidden': isHidden }">
    <div class="card" style="margin-bottom: 15px;">
      <header class="card-header">
        <p class="card-header-title">Preview</p>
      </header>
      <div class="card-content">
        <label :for="opts.id">{{ opts.label }}</label>
        <div v-for="(option, key) in opts.options">
          <label :for="option.value">{{ option.text }}</label>
          <input type="checkbox" :id="option.value" :value="option.value" v-model="opts.value" />
        </div>
      </div>
    </div>
    <div class="field">
      <label class="label" for="label">Label</label>
      <div class="control">
        <input id="label" class="input" :name="'form[' + index + '][label]'" v-model="opts.label" />
      </div>
    </div>
    <div class="field">
      <label class="label" for="value">Value</label>
      <div class="control">     
          <input id="value" class="input" :name="'form[' + index + '][value]'" v-model="opts.value" />
      </div>
    </div>
    <div class="field">
      <label class="label" for="id">ID</label>
      <div class="control">  
        <input id="id" class="input" :name="'form[' + index + '][id]'" v-model="opts.id" />
      </div>
    </div>
    <div class="field">
      <label class="label" for="class">Class</label>
      <div class="control">
        <input id="class" class="input" :name="'form[' + index + '][class]'" v-model="opts.class" />
      </div>
    </div>
    <div class="field">
      <label class="label" for="name">Name</label>
      <div class="control">
        <input id="name" class="input" :name="'form[' + index + '][name]'" v-model="opts.name" />
      </div>
    </div>
    <div class="field"> 
      <label class="label" for="required">Required</label>
      <div class="control">
        <input id="required" class="checkbox" :name="'form[' + index + '][required]'" type="checkbox" v-model="opts.required" />
      </div>
    </div>
    <input :name="'form[' + index + '][type]'" type="hidden" value="checkbox" />
    <input :name="'form[' + index + '][component]'" type="hidden" value="fbcheckbox" />
    <div>
      <div v-for="(option, key) in opts.options">
        <div v-if="key == 0" class="plus_container" @click="addOption(-1)"><svg id="plus" viewBox="0 0 1024 1024" width="20" height="20"><title>plus</title> <path fill="#444" d="M409.6 1024h204.8v-1024h-204.8v1024z" class="path1"></path> <path fill="#444" d="M0 409.6v204.8h1024v-204.8h-1024z" class="path2"></path></svg></div>
        <div @click="removeOption(key)"><svg id="cross" viewBox="0 0 1024 1024" width="20" height="20"><title>cross</title> <path d="M222.378 705.092l96.53 96.53 482.714-482.714-96.53-96.53-482.714 482.714zM318.908 222.378l-96.53 96.53 482.714 482.714 96.53-96.53-482.714-482.714z" class="path1"></path></svg></div>
        <div class="columns">
          <div class="column">
            <input class="input" placeholder="value" type="text" v-model="option.value" />
            <input type="hidden" :name="'form[' + index + '][options][' + key + '][value]'" :value="option.value" />
          </div>
          <div class="column">
            <input class="input" placeholder="option" type="text" v-model="option.text" />
            <input type="hidden" :name="'form[' + index + '][options][' + key + '][text]'" :value="option.text" />
          </div>
        </div>
        <div class="plus_container" @click="addOption(key)"><svg id="plus" viewBox="0 0 1024 1024" width="20" height="20"><title>plus</title> <path fill="#444" d="M409.6 1024h204.8v-1024h-204.8v1024z" class="path1"></path> <path fill="#444" d="M0 409.6v204.8h1024v-204.8h-1024z" class="path2"></path></svg></div>
      </div>
    </div>
  </div>
</article>`,
  data() {
    return {
      value: null,
      isHidden: true
    }
  },
  methods: {
    addOption: function(key) {
      Vue.set(this.opts, 'value', []);
      var key = key + 1;
      this.opts.options.splice(key, 0, {"value": null, "text": null});
    },
    removeOption: function(key) {
      this.opts.options.splice(key, 1);
    },
    selectBlock: function(index) {
      this.$emit('selectBlock', index);
    },
    removeBlock: function(index) {
      this.$emit('removeBlock', index);
    },
    toggleBlock: function() {
      this.isHidden = !this.isHidden;
    }
  },
  mounted() {
    if (typeof this.opts.options == 'undefined') {
      Vue.set(this.opts, 'options', [{"value": null,"text": null}]);
    }
    if (this.opts.value == "") {
      Vue.set(this.opts, 'value', []);
    } else if (typeof this.opts.value.split == 'function') {
      console.log('this.opts.value', this.opts.value);
      Vue.set(this.opts, 'value', this.opts.value.split(","));
    }
  }
});
Vue.component('fbradio', {
  props: ['opts', 'index'],
  template: `<article class="message">
  <div class="message-header">
    <p>Radio Button(s)</p>
    <div>
      <button @click="toggleBlock()" class="delete" aria-label="delete"></button>
      <button @click="removeBlock(index)" class="delete" aria-label="delete"></button>
    </div>
  </div>
  <div class="message-body" v-bind:class="{ 'is-hidden': isHidden }">
    <div class="card" style="margin-bottom: 15px;">
      <header class="card-header">
        <p class="card-header-title">Preview</p>
      </header>
      <div class="card-content">
        <label class="label" :for="opts.id">{{ opts.label }}</label>
        <div v-for="(option, key) in opts.options">
          <label class="radio" :for="option.value">
            <input type="radio" :id="option.value" :value="option.value" v-model="opts.value" />
            {{ option.text }}
          </label>
        </div>
      </div>
    </div>
    <div class="field">
      <label class="label" for="label">Label</label>
      <div class="control">
        <input id="label" class="input" :name="'form[' + index + '][label]'" v-model="opts.label" />
      </div>
    </div>
    <div class="field">
      <label class="label" for="value">Value</label>
      <div class="control">     
          <input id="value" class="input" :name="'form[' + index + '][value]'" v-model="opts.value" />
      </div>
    </div>
    <div class="field">
      <label class="label" for="id">ID</label>
      <div class="control">  
        <input id="id" class="input" :name="'form[' + index + '][id]'" v-model="opts.id" />
      </div>
    </div>
    <div class="field">
      <label class="label" for="class">Class</label>
      <div class="control">
        <input id="class" class="input" :name="'form[' + index + '][class]'" v-model="opts.class" />
      </div>
    </div>
    <div class="field">
      <label class="label" for="name">Name</label>
      <div class="control">
        <input id="name" class="input" :name="'form[' + index + '][name]'" v-model="opts.name" />
      </div>
    </div>
    <div class="field"> 
      <label class="label" for="required">Required</label>
      <div class="control">
        <input id="required" class="checkbox" :name="'form[' + index + '][required]'" type="checkbox" v-model="opts.required" />
      </div>
    </div>
    <input :name="'form[' + index + '][type]'" type="hidden" value="checkbox" />
    <input :name="'form[' + index + '][component]'" type="hidden" value="fbradio" />
    <div v-for="(option, key) in opts.options">
      <div v-if="key == 0" class="plus_container" @click="addOption(-1)"><svg id="plus" viewBox="0 0 1024 1024" width="20" height="20"><title>plus</title> <path fill="#444" d="M409.6 1024h204.8v-1024h-204.8v1024z" class="path1"></path> <path fill="#444" d="M0 409.6v204.8h1024v-204.8h-1024z" class="path2"></path></svg></div>
      <div @click="removeOption(key)"><svg id="cross" viewBox="0 0 1024 1024" width="20" height="20"><title>cross</title> <path d="M222.378 705.092l96.53 96.53 482.714-482.714-96.53-96.53-482.714 482.714zM318.908 222.378l-96.53 96.53 482.714 482.714 96.53-96.53-482.714-482.714z" class="path1"></path></svg></div>
      <div class="columns">
        <div class="column">
          <input class="input" placeholder="value" type="text" v-model="option.value" />
          <input type="hidden" :name="'form[' + index + '][options][' + key + '][value]'" :value="option.value" />
        </div>
        <div class="column">
          <input class="input" placeholder="option" type="text" v-model="option.text" />
          <input type="hidden" :name="'form[' + index + '][options][' + key + '][text]'" :value="option.text" />
        </div>
      </div>
      <div class="plus_container" @click="addOption(key)"><svg id="plus" viewBox="0 0 1024 1024" width="20" height="20"><title>plus</title> <path fill="#444" d="M409.6 1024h204.8v-1024h-204.8v1024z" class="path1"></path> <path fill="#444" d="M0 409.6v204.8h1024v-204.8h-1024z" class="path2"></path></svg></div>
    </div>
  </div>
</article>`,
  data() {
    return {
      value: null,
      isHidden: true
    }
  },
  methods: {
    addOption: function(key) {
      var key = key + 1;
      this.opts.options.splice(key, 0, {"value": "", "text": ""});
    },
    removeOption: function(key) {
      this.opts.options.splice(key, 1);
    },
    selectBlock: function(index) {
      this.$emit('selectBlock', index);
    },
    removeBlock: function(index) {
      this.$emit('removeBlock', index);
    },
    toggleBlock: function() {
      this.isHidden = !this.isHidden;
    }
  },
  mounted() {
    if (typeof this.opts.options == 'undefined') {
      Vue.set(this.opts, 'options', [{"value": null,"text": null}]);
      Vue.set(this.opts, 'value', []);
    }
  }
});
Vue.component('fbtextarea', {
  props: ['opts', 'index'],
  template: `<article class="message">
  <div class="message-header">
    <p>Textarea</p>
    <div>
      <button @click="toggleBlock()" class="delete" aria-label="delete"></button>
      <button @click="removeBlock(index)" class="delete" aria-label="delete"></button>
    </div>
  </div>
  <div class="message-body" v-bind:class="{ 'is-hidden': isHidden }">
    <div class="card" style="margin-bottom: 15px;">
      <header class="card-header">
        <p class="card-header-title">Preview</p>
      </header>
      <div class="card-content">
        <div class="field">
          <label class="label" :for="opts.id">{{ opts.label }}</label>
          <div class="control" :class="opts.class">
            <textarea
              class="textarea"
              :id="opts.id"
              v-model="opts.value"
              :name_test="opts.name"
              :rows="opts.rows"
              :cols="opts.cols"
              :placeholder="opts.placeholder"
              :disabled="opts.disabled"
              :required="opts.required"
            >
            </textarea>
          </div>
        </div>
      </div>
    </div>
    <div class="field">
      <label class="label" for="label">Label</label>
      <div class="control">
        <input id="label" class="input" :name="'form[' + index + '][label]'" v-model="opts.label" />
      </div>
    </div>
    <div class="field">
      <label class="label" for="value">Value</label>
      <div class="control">     
          <input id="value" class="input" :name="'form[' + index + '][value]'" v-model="opts.value" />
      </div>
    </div>
    <div class="field">
      <label class="label" for="id">ID</label>
      <div class="control">  
        <input id="id" class="input" :name="'form[' + index + '][id]'" v-model="opts.id" />
      </div>
    </div>
    <div class="field">
      <label class="label" for="class">Class</label>
      <div class="control">
        <input id="class" class="input" :name="'form[' + index + '][class]'" v-model="opts.class" />
      </div>
    </div>
    <div class="field">
      <label class="label" for="name">Name</label>
      <div class="control">
        <input id="name" class="input" :name="'form[' + index + '][name]'" v-model="opts.name" />
      </div>
    </div>
    <div class="field"> 
      <label class="label" for="rows">Rows</label>
      <div class="control">
        <input class="input" id="rows" :name="'form[' + index + '][rows]'" v-model="opts.rows" />  
      </div>
    </div>
    <div class="field"> 
      <label class="label" for="cols">Cols</label>
      <div class="control">
        <input class="input" id="cols" :name="'form[' + index + '][cols]'" v-model="opts.cols" />   
      </div>
    </div>
    <div class="field"> 
      <label class="label"  for="disabled">Disabled</label>
      <div class="control">
        <input id="disabled" class="checkbox" :name="'form[' + index + '][disabled]'" type="checkbox" v-model="opts.disabled" />    
      </div>
    </div>
    <div class="field"> 
      <label class="label" for="required">Required</label>
      <div class="control">
        <input id="required" class="checkbox" :name="'form[' + index + '][required]'" type="checkbox" v-model="opts.required" />
      </div>
    </div>
    <input :name="'form[' + index + '][type]'" type="hidden" v-model="opts.type" />
  </div>
</article>`,
  data() {
    return {
      value: null,
      isHidden: true
    }
  },
  methods: {
    addOption: function(key) {
      var key = key + 1;
      this.opts.options.splice(key, 0, {"value": "", "text": ""});
    },
    removeOption: function(key) {
      this.opts.options.splice(key, 1);
    },
    selectBlock: function(index) {
      this.$emit('selectBlock', index);
    },
    removeBlock: function(index) {
      this.$emit('removeBlock', index);
    },
    toggleBlock: function() {
      this.isHidden = !this.isHidden;
    }
  }
});
Vue.component('formup', {
  props: ['data', 'formupdebug'],
  template: `
  <div>
    <div class="columns">
      <div class="column is-uppercase" @click="insertBlock('fbtext')">
        <p class="bd-notification is-primary">Add Input</p>
      </div>
      <div class="column is-uppercase" @click="insertBlock('fbtextarea')">
        <p class="bd-notification is-primary">Add Text Area</p>
      </div>
      <div class="column is-uppercase" @click="insertBlock('fbselect')">
        <p class="bd-notification is-primary">Add Select Dropdown</p>
      </div>
      <div class="column is-uppercase" @click="insertBlock('fbradio')">
        <p class="bd-notification is-primary">Add Radio Button(s)</p>
      </div>
      <div class="column is-uppercase" @click="insertBlock('fbcheckbox')">
        <p class="bd-notification is-primary">Add Checkbox(es)</p>
      </div>
    </div>
    <div id="listWithHandle">
      <component
        v-for="(item, key) in items"
        :key="key"
        :index="key"
        :is="item.component"
        :opts="item"
        @removeBlock="removeBlock"
      >
      </component>
    </div>
  </div>`,
  data() {
    return {
      items: null,
      current_selection: null,
      selection_text: null
    };
  },
  methods: {
    insertBlock(blocktype) {
      this.items.push({component: blocktype, value: ""});
    },
    removeBlock(index) {
      console.log('removeBlock index:', index);
      if (index == 0) {
        this.items = null;
      } else {
        this.items.splice(index, 1);
      }
    }
  },
  beforeMount: function () {
    if (this.items) {
      this.items = this.data;
    } else {
      this.items = [{
        "component": "fbsettings",
        "action": null,
        "method": "POST",
        "acceptcharset": "UTF-8",
        "enctype": "multipart/form-data",
        "name": null,
        "novalidate": null,
        "target": null,
        "autocomplete": null,
        "id": "main_form",
        "class": null,
      }];
    }
    if (this.formupdebug) {
      console.log(this.items);
    }
  },
  mounted() {
      console.log('form builder mounted');
  //   EditorContainer.destroy();
  //   EditorContainer.create();
  //   Sortable.create(listWithHandle, {
  //     handle: '.my-handle',
  //     animation: 150
  //   });
  }
});

window.EditorContainer = new class {
  constructor() {
    this.editors = [];
    this.options = {
      buttonLabels: 'fontawesome',
      toolbar: {
        buttons: ['bold', 'italic', 'underline', 'anchor', 'list-extension']
      },
      extensions: {
        'list-extension': new MediumEditorList()
      },
      mediumEditorList: {
        newParagraphTemplate: '',
        buttonTemplate: '<i class="fa fa-list" aria-hidden="true"></i>',
        addParagraphTemplate: '',
        isEditable: true,
        autofocus: true
      }
    };
  }
  create() {
    var editorElements = Array.from(document.getElementsByClassName("editit"));
    if (editorElements.length) {
      for (var i = 0; i < editorElements.length; i++) {
        var editor = new MediumEditor(document.getElementById(editorElements[i].id), this.options);
        this.editors.push(editor);
      }
    }
  }
  destroy() {
    if (this.editors.length) {
      for (var i = 0; i < this.editors.length; i++) {
        this.editors[i].destroy();
      }
      this.editors = null;
      this.editors = [];
    }
  }
}

Vue.component('heading1', {
  props: ['opts', 'index'],
  template: `<div>
              <span class="my-handle">::</span>
              <div @click="removeBlock(index)"><svg id="cross" viewBox="0 0 1024 1024" width="20" height="20"><title>cross</title> <path d="M222.378 705.092l96.53 96.53 482.714-482.714-96.53-96.53-482.714 482.714zM318.908 222.378l-96.53 96.53 482.714 482.714 96.53-96.53-482.714-482.714z" class="path1"></path></svg></div>
              <h1 class="heading1 editit" editable @blur="update" :id="'h1_'+ index" :class="opts.class" v-html="opts.value"></h1>
              <input type="hidden" :name="'page[heading1_' + index + ']'" v-model="opts.value" />
            </div>`,
  data() {
    return {
      value: null
    }
  },
  mounted() {
    EditorContainer.destroy();
    EditorContainer.create();
  },
  methods: {
    update: function(event){
      this.opts.value = event.target.innerHTML;
    },
    selectBlock: function(index) {
      this.$emit('selectBlock', index);
    },
    removeBlock: function(index) {
      this.$emit('removeBlock', index);
    }
  }
});
Vue.component('paragraph', {
  props: ['opts', 'index'],
  template: `<div>
              <span class="my-handle">::</span>
              <div @click="removeBlock(index)"><svg id="cross" viewBox="0 0 1024 1024" width="20" height="20"><title>cross</title> <path d="M222.378 705.092l96.53 96.53 482.714-482.714-96.53-96.53-482.714 482.714zM318.908 222.378l-96.53 96.53 482.714 482.714 96.53-96.53-482.714-482.714z" class="path1"></path></svg></div>
              <p autofocus ref="paragraph" class="paragraph editit" editable @blur="update" :id="'p_'+ index" :class="opts.class" v-html="opts.value"></p>
              <input type="hidden" :name="'page[paragraph_' + index + ']'" v-model="opts.value" />
            </div>`,
  data() {
    return {
      value: null,
      editor: null
    }
  },
  mounted() {
    EditorContainer.destroy();
    EditorContainer.create();
  },
  methods: {
    update: function(event){
      this.opts.value = event.target.innerHTML;
    },
    selectBlock: function(index) {
      this.$emit('selectBlock', index);
    },
    removeBlock: function(index) {
      this.$emit('removeBlock', index);
    }
  }
});
Vue.component('quote', {
  props: ['opts', 'index'],
  template: `<div>
              <div @click="removeBlock(index)"><svg id="cross" viewBox="0 0 1024 1024" width="20" height="20"><title>cross</title> <path d="M222.378 705.092l96.53 96.53 482.714-482.714-96.53-96.53-482.714 482.714zM318.908 222.378l-96.53 96.53 482.714 482.714 96.53-96.53-482.714-482.714z" class="path1"></path></svg></div>
              <blockquote autofocus class="blockquote editit" editable @blur="update" :id="'blockquote_'+ index" :class="opts.class" v-html="opts.value"></blockquote>
              <input type="hidden" :name="'page[quote_' + index + ']'" v-model="opts.value" />
            </div>`,
  data() {
    return {
      value: null,
      editor: null
    }
  },
  mounted() {
    EditorContainer.destroy();
    EditorContainer.create();
  },
  methods: {
    update: function(event){
      this.opts.value = event.target.innerHTML;
    },
    selectBlock: function(index) {
      this.$emit('selectBlock', index);
    },
    removeBlock: function(index) {
      this.$emit('removeBlock', index);
    }
  }
});
Vue.component('unordered', {
  props: ['opts', 'index'],
  template: `<div>
              <div @click="removeBlock(index)"><svg id="cross" viewBox="0 0 1024 1024" width="20" height="20"><title>cross</title> <path d="M222.378 705.092l96.53 96.53 482.714-482.714-96.53-96.53-482.714 482.714zM318.908 222.378l-96.53 96.53 482.714 482.714 96.53-96.53-482.714-482.714z" class="path1"></path></svg></div>
              <ul autofocus class="unordered editit" editable @blur="update" :id="'ul_'+ index" :class="opts.class" v-html="opts.value"></ul>
              <input type="hidden" :name="'page[unordered_' + index + ']'" v-model="opts.value" />
            </div>`,
  data() {
    return {
      value: null,
      editor: null
    }
  },
  mounted() {
    console.log('value', this.opts.value.length);
    if (this.opts.value.length == 0) {
      this.opts.value = '<li></li>'
    }
    EditorContainer.destroy();
    EditorContainer.create();
  },
  methods: {
    update: function(event){
      this.opts.value = event.target.innerHTML;
    },
    selectBlock: function(index) {
      this.$emit('selectBlock', index);
    },
    removeBlock: function(index) {
      this.$emit('removeBlock', index);
    }
  }
});
Vue.component('ordered', {
  props: ['opts', 'index'],
  template: `<div>
              <div @click="removeBlock(index)"><svg id="cross" viewBox="0 0 1024 1024" width="20" height="20"><title>cross</title> <path d="M222.378 705.092l96.53 96.53 482.714-482.714-96.53-96.53-482.714 482.714zM318.908 222.378l-96.53 96.53 482.714 482.714 96.53-96.53-482.714-482.714z" class="path1"></path></svg></div>
              <ol autofocus class="ordered editit" editable @blur="update" :id="'ul_'+ index" :class="opts.class" v-html="opts.value"></ol>
              <input type="hidden" :name="'page[ordered_' + index + ']'" v-model="opts.value" />
            </div>`,
  data() {
    return {
      value: null,
      editor: null
    }
  },
  mounted() {
    console.log('value', this.opts.value.length);
    if (this.opts.value.length == 0) {
      this.opts.value = '<li></li>'
    }
    EditorContainer.destroy();
    EditorContainer.create();
  },
  methods: {
    update: function(event){
      this.opts.value = event.target.innerHTML;
    },
    selectBlock: function(index) {
      this.$emit('selectBlock', index);
    },
    removeBlock: function(index) {
      this.$emit('removeBlock', index);
    }
  }
});
Vue.component('markup', {
  props: ['data', 'markupdebug'],
  template: `
  <div>
    <nav class="main-nav" id="main-nav">
      <ul id="main-nav-list">
        <li>
          <a href="#" @click="insertBlock('heading1')">
            <svg viewBox="0 0 100 100" class="icon">
              <use xlink:href="#leaf"></use>
            </svg>
          </a>
        </li>
        <li>
          <a href="#" @click="insertBlock('paragraph')">
            <svg viewBox="0 0 100 100" class="icon">
              <use xlink:href="#maple-leaf"></use>
            </svg>
          </a>
        </li>
        <li>
          <a href="#" @click="insertBlock('quote')">
            <svg viewBox="0 0 100 100" class="icon">
              <use xlink:href="#clover"></use>
            </svg>
          </a>
        </li>
        <li>
          <a href="#" @click="insertBlock('unordered')">
            <svg viewBox="0 0 100 100" class="icon">
              <use xlink:href="#tomato"></use>
            </svg>
          </a>
        </li>
        <li>
          <a href="#" @click="insertBlock('ordered')">
            <svg viewBox="0 0 100 100" class="icon">
              <use xlink:href="#tomato"></use>
            </svg>
          </a>
        </li>
        <li>
          <a href="#" @click="insertBlock('unordered')">
            <svg viewBox="0 0 100 100" class="icon">
              <use xlink:href="#tomato"></use>
            </svg>
          </a>
        </li>
      </ul>
    </nav>
    <div id="listWithHandle">
      <component
        v-for="(item, key) in items"
        :key="key"
        :index="key"
        :is="item.component"
        :opts="item"
        @removeBlock="removeBlock"
      >
      </component>
    </div>
  </div>`,
  data() {
    return {
      items: [],
      current_selection: null,
      selection_text: null
    };
  },
  methods: {
    insertBlock(blocktype) {
      console.log('insertBlock blocktype:', blocktype);
      this.items.push({component: blocktype, value: ""});
    },
    removeBlock(index) {
      console.log('removeBlock index:', index);
      this.items.splice(index, 1);
    }
  },
  beforeMount: function () {
    if (this.data) {
      this.items = this.data;
    } else {
      this.items = [];
    }
    if (this.fbdebug) {
      console.log(this.items);
    }
  },
  mounted() {
    EditorContainer.destroy();
    EditorContainer.create();
    // Sortable.create(listWithHandle, {
    //   handle: '.my-handle',
    //   animation: 150
    // });
  }
});

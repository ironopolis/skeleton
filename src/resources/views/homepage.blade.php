@extends('skeleton::layouts.app')

@section('content')
  <homepage :homepagedebug="false" :data="{{ $data or '{}' }}" :settings="{{ json_encode($skeleton) }}"></homepage>
@endsection

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Website Enquiry</title>
</head>

<body>

  <h1>Website Enquiry</h1>
  <p><strong>Company: </strong>{{ $company }}</p>
  <p><strong>Name: </strong>{{ $name }}</p>
  <p><strong>Email: </strong>{{ $email }}</p>
  <p><strong>Phone: </strong>{{ $phone }}</p>
  <p><strong>Question: </strong>{{ $question }}</p>
  
</body>
</html>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>{{ $title or '' }}</title>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="{{ $keywords or '' }}">
        <meta name="description" content="{{ $description or '' }}">
        <meta name="google-site-verification" content="fBUgzEo-gBAz0a5bovQAuWAK9jGzvQNnl2D1NnCbxjk" />
        <link href="/css/font.css" rel="stylesheet" />
        <link href="/css/app.css" rel="stylesheet" />
        <link rel='canonical' href="{{ url()->current() }}" />
        <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png?v=JykBkddmnk">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png?v=JykBkddmnk">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png?v=JykBkddmnk">
        <link rel="manifest" href="/favicons/site.webmanifest?v=JykBkddmnk">
        <link rel="mask-icon" href="/favicons/safari-pinned-tab.svg?v=JykBkddmnk" color="#5bbad5">
        <link rel="shortcut icon" href="/favicons/favicon.ico?v=JykBkddmnk">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="msapplication-config" content="/favicons/browserconfig.xml?v=JykBkddmnk">
        <meta name="theme-color" content="#ffffff">
    </head>
    <body>
        <div id="app">
            @if (Request::path() == '/' && !empty($data->banner))
            <banner-component :bannerdebug="false" :data="{{ $data }}"></banner-component>
            @endif
            @if (false)
            <nav-standard-component :settings="{{ json_encode($skeleton) }}"></nav-standard-component>
            @endif
            <div>
                <nav class="nav-standard nav-standard--border-bottom is-bg-secondary">
                    <div class="nav-standard__item nav-standard__item--small">
                        <a href="/" class="nav-standard__logo">
                            <div>
                                {!! $skeleton->svg_logo or '' !!}
                            </div>
                        </a>
                    </div> 
                    <div class="nav-standard__item nav-standard__item--large">
                        <nav-search-component :settings="{{ json_encode($skeleton) }}"></nav-search-component>
                    </div> 
                    <div class="nav-standard__item nav-standard__item--small nav-standard__item-menu">
                        <div class="nav-standard__menu-button" @click="showMenu(); toggleBlur(visible);">
                            <div type="button" class="nav-standard__burger" :class="{'nav-standard__burger--open': visible}">
                                <span></span>
                            </div>
                            <p class="nav-standard__menu-text has-text-white">MENU</p>
                        </div>
                    </div> 
                    {!! $menu !!}
                </nav>
            </div>


            <div id="content">
                @yield('content')
            </div>
            <footer-component :settings="{{ json_encode($skeleton) }}"></footer-component>
        </div>
        <script type="text/javascript" src="/js/app.js"></script>
        <script type="text/javascript" src="/js/courses.js"></script>
        @if ($skeleton->display_map == true && (Request::path() == '/' || Request::path() == 'contact'))
        <script>
            function initMap() {
                let 
                styled = [{"elementType":"geometry","stylers":[{"color":"#f5f5f5"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#f5f5f5"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.fill","stylers":[{"color":"#bdbdbd"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#dadada"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"transit.station","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#c9c9c9"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]}],
                myLatLng = {lat: 53.417760, lng: -1.384822},
                map = new google.maps.Map(document.getElementById("home-map"), {
                    center: myLatLng,
                    scrollwheel: false,
                    styles: styled,
                    disableDefaultUI: true,
                    zoom: 14
                }),
                marker = new google.maps.Marker({
                    map: map,
                    icon:"/storage/placeholders/marker.png",
                    position: myLatLng,
                    title: 'Elec Safety'
                });
            }
        </script>
        <script async defer type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyACYVgz2rWpkyCdC64n1Dh-v36YYsKk7y4&amp;callback=initMap"></script>
        @endif
        <script type="text/javascript" src="https://secure.leadforensics.com/js/113735.js"></script>
        <noscript><img src="https://secure.leadforensics.com/113735.png" alt="" style="display:none;" /></noscript>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            ga('create', '{{ $skeleton->apikey1 or '' }}', 'auto');
            ga('send', 'pageview');
        </script>
    </body>
</html>

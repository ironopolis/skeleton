@extends('skeleton::layouts.app')

@section('content')
  <?php
  $json = json_decode($data);
  ?>
  @if (!empty($json->hero) && $json->hero->active)
  <hero-component :hero="{{ json_encode($json->hero) }}" :settings="{{ json_encode($skeleton) }}"></hero-component>
  @endif
  @if (!empty($json->banner) && $json->banner->active)
  <banner-component :data="{{ json_encode($json) }}"></banner-component>
  @endif
  @if (!empty($content))
  <section class="section">
    <div class="container">
      <div class="content">
        {!! $content !!}
      </div>
    </div>
  </section>
  @endif
  <page :pagedebug="false" :data="{{ $data }}" :settings="{{ json_encode($skeleton) }}"></page>
@endsection
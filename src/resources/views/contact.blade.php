@extends('skeleton::layouts.app')

@section('content')
  <?php
    if (!empty($data)) {
      $json = json_decode($data);
    }
    ?>
    @if (!empty($json->hero) && $json->hero->active)
    <hero-component :hero="{{ json_encode($json->hero) }}" :settings="{{ json_encode($skeleton) }}"></hero-component>
    @endif
  <section class="section">
    <div class="container">
      @if (session()->has('message'))
        <div class="notification is-primary">
          {{ session('message') }}
        </div>
      @endif
      @if (count($errors))
          <ul class="notification">
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      @endif
      <contact :contactdebug="false" :data="{{ $data }}" :settings="{{ json_encode($skeleton) }}"></contact>
    </div>
  </section>
  <section class="section">
    <div class="container breaks">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 54.8 15.5" width="70"><path fill="#f39200" d="M0 0v15.5h54.8L39.3 0z"></path></svg>
      <p class="title is-2">Come and visit us!</p>
      <div id="home-map" class="container box"></div>
    </div>
  </section>
@endsection
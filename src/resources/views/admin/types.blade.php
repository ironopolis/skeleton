@extends('skeleton::layouts.admin')

@section('content')
<div id="app" class="container">
  <section class="section">
    <div class="container">
      <nav class="breadcrumb" aria-label="breadcrumbs">
        <ul>
          <li><a href="/admin">Dashboard</a></li>
          <li class="is-active"><a href="/admin/types" aria-current="page">Types Builder</a></li>
        </ul>
      </nav>
      <h1 class="title">Types Builder</h1>
      <h2 class="subtitle">
          Create routes that relate to groups of blocks of a particular type for display in various layouts such as responsive grids.
      </h2>
    </div>
  </section>
  <section class="section">
        <div class="container">
          <div class="columns is-multiline">
            @foreach($types as $type)
              <div class="column is-half">
                  <div class="card">
                      <div class="card-content">
                          <div class="content">
                              <a href="/admin/type/build/{{ $type->type }}">{{ $type->type }}</a>
                          </div>
                      </div>
                  </div>
              </div>
            @endforeach
          </div>
        </div>
    </section>

</div>
@endsection

@extends('skeleton::layouts.admin')

@section('content')
  <div class="container">
    <div class="columns">
      <div class="column is-2">
        <nav class="breadcrumb" aria-label="breadcrumbs">
          <ul>
            <li><a href="/admin">Dashboard</a></li>
            <li class="is-active"><a href="#" aria-current="page">Pages</a></li>
          </ul>
        </nav>
        <h1 class="title">Pages</h1>
        <a href="/admin/page/create" class="button is-info">Create New Page</a>
      </div>
    </div>
    <div class="box">
      <form method="GET" action="/admin/pages">
        @csrf
        <div class="columns section">
            <div class="column is-4">
                <div class="field is-horizontal">
                  <div class="field-label is-normal">
                    <label class="label">Type</label>
                  </div>
                  <div class="field-body">
                    <div class="field">
                      <div class="select">
                        <select name="type">
                          <option>Filter by type</option>
                          @foreach ($types as $type)
                          <option value="{{ $type->type }}" {{ $current_type == $type->type? 'selected' : '' }}>{{ $type->type }}</option>
                          @endforeach
                        </select>
                      </div>
                      <input class="button" type="submit" value="Filter">
                    </div>
                  </div>
                </div>
            </div>
            <div class="column is-4">
              <div class="field is-horizontal">
                <div class="field-label is-normal">
                  <label class="label">Search</label>
                </div>
                <div class="field-body">
                  <div class="field">
                    <p class="control">
                      <input class="input" name="search" type="text" placeholder="Search pages">
                    </p>
                  </div>
                </div>
                <input class="button" type="submit" value="Search">
              </div>
            </div>
        </div>
      </form>
    {{ $pages->appends(request()->input())->links() }}
    </div>
    <table class="table">
      <thead>
        <tr>
          <!-- <th>Select</th> -->
          <th><abbr title="Position">ID</abbr></th>
          <th>Route</th>
          <th>Type</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <!-- <th>Select</th> -->
          <th><abbr title="Position">ID</abbr></th>
          <th>Route</th>
          <th>Type</th>
          <th>Actions</th>
        </tr>
      </tfoot>
      <tbody>
        @foreach($pages as $page)
          <tr>
            <!-- <th><input type="checkbox"></th> -->
            <th>{{ $page->id }}</th>
            <td><a href="/admin/page/build/{{ $page->id }}">{{ $page->route }}</td>
            <th>{{ $page->type }}</th>
            <td>
              @if ($page->locked)
              <form method="POST" action="/admin/page/unlock/{{ $page->id }}" style="display: inline-block;">
                  @csrf
                  <label>
                      <input style="display:none;" type="submit" />
                      <svg style="width:24px;height:24px;cursor:pointer;" viewBox="0 0 24 24">
                          <path fill="#000000" d="M12,17A2,2 0 0,0 14,15C14,13.89 13.1,13 12,13A2,2 0 0,0 10,15A2,2 0 0,0 12,17M18,8A2,2 0 0,1 20,10V20A2,2 0 0,1 18,22H6A2,2 0 0,1 4,20V10C4,8.89 4.9,8 6,8H7V6A5,5 0 0,1 12,1A5,5 0 0,1 17,6V8H18M12,3A3,3 0 0,0 9,6V8H15V6A3,3 0 0,0 12,3Z" />
                      </svg>
                  </label>
              </form>
              @else
              <form method="POST" action="/admin/page/lock/{{ $page->id }}" style="display: inline-block;">
                  @csrf
                  <label>
                      <input style="display:none;" type="submit" />
                      <svg style="width:24px;height:24px;cursor:pointer;" viewBox="0 0 24 24">
                          <path fill="#000000" d="M18,8A2,2 0 0,1 20,10V20A2,2 0 0,1 18,22H6C4.89,22 4,21.1 4,20V10A2,2 0 0,1 6,8H15V6A3,3 0 0,0 12,3A3,3 0 0,0 9,6H7A5,5 0 0,1 12,1A5,5 0 0,1 17,6V8H18M12,17A2,2 0 0,0 14,15A2,2 0 0,0 12,13A2,2 0 0,0 10,15A2,2 0 0,0 12,17Z" />
                      </svg>
                  </label>
              </form>
              @endif
              @if (!$page->locked && $page->type != "course")
              <form method="POST" action="/admin/page/update/{{ $page->id }}" style="display: inline-block;">
                  @csrf
                  <label>
                      <input style="display:none;" type="submit" />
                      <svg style="width:24px;height:24px;cursor:pointer;" viewBox="0 0 24 24">
                          <path fill="#000000" d="M17.65,6.35C16.2,4.9 14.21,4 12,4A8,8 0 0,0 4,12A8,8 0 0,0 12,20C15.73,20 18.84,17.45 19.73,14H17.65C16.83,16.33 14.61,18 12,18A6,6 0 0,1 6,12A6,6 0 0,1 12,6C13.66,6 15.14,6.69 16.22,7.78L13,11H20V4L17.65,6.35Z" />
                      </svg>
                  </label>
              </form>
              @endif
              <form method="POST" action="/admin/page/delete/{{ $page->id }}" style="display: inline-block;">
                  @csrf
                  <label>
                      <input style="display:none;" type="submit" />
                      <svg style="width:24px;height:24px;cursor:pointer;" viewBox="0 0 24 24">
                          <path fill="#000000" d="M19,4H15.5L14.5,3H9.5L8.5,4H5V6H19M6,19A2,2 0 0,0 8,21H16A2,2 0 0,0 18,19V7H6V19Z" />
                      </svg>
                  </label>
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
@endsection

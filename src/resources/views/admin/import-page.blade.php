@extends('skeleton::layouts.admin')

@section('content')
<form method="POST">
  @csrf
  <section class="section">
    <div class="container">
      <div class="card">
        <div class="card-image">
          <figure class="image is-4by3">
            <img src="{{ !empty($post->thumbnail) ? $post->thumbnail : 'https://bulma.io/images/placeholders/1280x960.png' }}" alt="Placeholder image">
          </figure>
        </div>
        <div class="card-content">
          <div class="media">
            <div class="media-left">
              <figure class="image is-48x48">
                <img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
              </figure>
            </div>
            <div class="media-content">
              <p class="title is-4">{{ $post->post_title }}</p>
              <p class="subtitle is-6">Authored by @rebeccafrain</p>
            </div>
          </div>
        
          <div class="content">
            {!! $post->post_content !!}
            @if (!empty($post->images))
              <table class="table">
                <thead>
                  <tr>
                    <th>Image</th>
                    <th>Import?</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Image</th>
                    <th>Import?</th>
                  </tr>
                </tfoot>
                <tbody>
                    @foreach ($post->images as $image) 
                      <tr>
                        <th><img src="{{ !empty($image) ? $image : 'https://bulma.io/images/placeholders/1280x960.png' }}" alt="Placeholder image"></th>
                        <td><input type="checkbox"></td>
                      </tr>
                    @endforeach
                </tbody>
              </table>
            @endif
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="section">
    <div class="container">
      <input class="button" type="submit" name="import" value="IMPORT PAGE">
    </div>
  </section>
</form>
@endsection

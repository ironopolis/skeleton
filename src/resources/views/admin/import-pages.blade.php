@extends('skeleton::layouts.admin')

@section('content')

<section class="section">
  <div class="container">
    <div class="box">
      {{ $posts->links() }}
    </div>
    <table class="table">
      <thead>
        <tr>
          <th><abbr title="Position">ID</abbr></th>
          <th>Page</th>
            <th>Page Content</th>
            <th>Published</th>
            <th>Select</th>
          </tr>
      </thead>
      <tfoot>
        <tr>
          <th><abbr title="Position">ID</abbr></th>
            <th>Page</th>
            <th>Page Content</th>
            <th>Published</th>
            <th>Select</th>
          </tr>
      </tfoot>
      <tbody>
        @foreach($posts as $post)
        <tr>
          <th>{{ $post->ID }}</th>
          <td><a href="/admin/pages/import-page/{{ $post->ID }}" title="{{ $post->post_title }}">{{ $post->post_title }}</td>
          <th>{!! str_replace('container', '', $post->post_content) !!}</th>
          <td>{{ $post->post_status == 'publish' ? 'Published' : 'Draft' }}</td>
          <td><input type="checkbox"></a></td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <div class="box">
      {{ $posts->links() }}
    </div>
  </div>
</section>
@endsection

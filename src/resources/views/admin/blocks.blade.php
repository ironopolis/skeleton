@extends('skeleton::layouts.admin')

@section('content')

  <div class="container">
    <div class="columns">
      <div class="column is-2">
        <h1 class="title">Blocks</h1>
      </div>
      <div class="column">
        <a href="/admin/block/create" class="button is-info">Create New Block</a>
      </div>
    </div>
    <h2 class="subtitle">
      Add content to your new block. Then you need to decide if this block is to be a page in its own right (that can be accessed via the slug you specify) or simply a standalone block that will be displayed within an existing page or component.
    </h2>
    @if (session()->has('message'))
    <div class="notification is-primary">
      {{ session('message') }}
    </div>
    @endif
    @if (count($blocks))
    <div class="columns">
      <div class="column is-4">
        <nav class="breadcrumb" aria-label="breadcrumbs">
          <ul>
            <li><a href="/admin">Dashboard</a></li>
            <li class="is-active"><a href="#" aria-current="page">Blocks</a></li>
          </ul>
        </nav>
      </div>
      <div class="column">
        <div class="field is-horizontal">
          <div class="field-label is-normal">
            <label class="label">Type</label>
          </div>
          <div class="field-body">
            <div class="field">
              <div class="select">
                <select>
                  <option>Filter by type</option>
                  @foreach ($types as $type)
                  <option>{{ $type->type }}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="column">
        <div class="field is-horizontal">
          <div class="field-label is-normal">
            <label class="label">Search</label>
          </div>
          <div class="field-body">
            <div class="field">
              <p class="control">
                <input class="input" type="text" placeholder="Search blocks">
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="box">
      {{ $blocks->links() }}
    </div>
    <table class="table">
      <thead>
        <tr>
          <th><abbr title="Position">ID</abbr></th>
          <th>Block</th>
          <th>Type</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <th><abbr title="Position">ID</abbr></th>
          <th>Block</th>
          <th>Type</th>
          <th>Actions</th>
        </tr>
      </tfoot>
      <tbody>
        @foreach($blocks as $block)
          <tr>
            <th>{{ $block->id }}</th>
            <td><a href="/admin/block/{{ $block->id }}">{{ $block->title }}</td>
            <td><a href="/admin/block/{{ $block->id }}">{{ $block->type }}</td>
            <td>
              <a href="/admin/block/delete/{{$block->id}}" class="delete"></a>
              <svg style="width:24px;height:24px;cursor:pointer;" viewBox="0 0 24 24">
                  <path class="refresh" fill="rgba(10, 10, 10, 0.2)" d="M17.65,6.35C16.2,4.9 14.21,4 12,4A8,8 0 0,0 4,12A8,8 0 0,0 12,20C15.73,20 18.84,17.45 19.73,14H17.65C16.83,16.33 14.61,18 12,18A6,6 0 0,1 6,12A6,6 0 0,1 12,6C13.66,6 15.14,6.69 16.22,7.78L13,11H20V4L17.65,6.35Z" />
              </svg>
              <svg style="width:24px;height:24px;cursor:pointer;" viewBox="0 0 24 24">
                  <path class="duplicate" fill="rgba(10, 10, 10, 0.2)" d="M11,17H4A2,2 0 0,1 2,15V3A2,2 0 0,1 4,1H16V3H4V15H11V13L15,16L11,19V17M19,21V7H8V13H6V7A2,2 0 0,1 8,5H19A2,2 0 0,1 21,7V21A2,2 0 0,1 19,23H8A2,2 0 0,1 6,21V19H8V21H19Z" />
              </svg>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    <div class="box">
      {{ $blocks->links() }}
    </div>
    @else
      <p>You have no blocks. Use the button above to create one!</p>
    @endif
  </div>

@endsection
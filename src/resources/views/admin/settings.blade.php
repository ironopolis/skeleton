@extends('skeleton::layouts.admin')

@section('content')

<div class="container">
  <section class="section">
    <div class="container">
      <nav class="breadcrumb" aria-label="breadcrumbs">
        <ul>
          <li><a href="/admin">Dashboard</a></li>
          <li class="is-active"><a href="#" aria-current="page">Settings</a></li>
        </ul>
      </nav>
      <h1 class="title">Settings</h1>
      <h2 class="subtitle">
        Configure your site settings.
      </h2>
      <form method="POST" action="/admin/store">
        @csrf
        <settings-editor :settingsdebug="true" :settings="{{ $settings }}"></settings-editor>
      </form>
    </div>
  </section>
</div>
@endsection
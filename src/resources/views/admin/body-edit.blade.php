@extends('skeleton::layouts.admin')

@section('content')
<div id="app" class="container">

  <section class="section">
    <div class="container">
      <h1 class="title">Page Builder</h1>
      <h2 class="subtitle">
        A page builder that can be used to map <strong>your data</strong> to pages on your site.
      </h2>
    </div>
  </section>
</div>
@endsection
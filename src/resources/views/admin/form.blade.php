@extends('skeleton::layouts.admin')

@section('content')
<div class="container">
  <section class="section">
    <div class="container">
      @if (session()->has('message'))
      <div class="notification is-primary">
        {{ session('message') }}
      </div>
      @endif
      <nav class="breadcrumb" aria-label="breadcrumbs">
        <ul>
          <li><a href="/admin">Dashboard</a></li>
          <li><a href="/admin/forms">Forms</a></li>
          <li class="is-active"><a href="#" aria-current="page">Form</a></li>
        </ul>
      </nav>
      <h1 class="title">Form</h1>
      <h2 class="subtitle">
        Create your form. Choose where it gets sent.
      </h2>
      <form method="POST" action="/admin/form/store" enctype="multipart/form-data">
        @csrf
        <form-editor :formdebug="true" :data="{{ $form or 'null' }}"></form-editor>
      </form>
    </div>
  </section>
</div>
@endsection


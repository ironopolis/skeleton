@extends('skeleton::layouts.admin')

@section('content')
<div id="app" class="container">
  <markup :markupdebug="false"></markup>
  <section class="section">
    <div class="container">
      <h1 class="title">Content Builder</h1>
      <h2 class="subtitle">
      Create content that <strong>captures the interest</strong> of your users.
      </h2>
    </div>
  </section>
</div>
@endsection

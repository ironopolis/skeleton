@extends('skeleton::layouts.admin')

<?php
  //dd($postmeta);
?>

@section('content')
<div id="app" class="container">

  @foreach ($post as $post_item)
  <section class="section">
    <div class="card">
      <header class="card-header">
        <p class="card-header-title">
           Content for {{ $post_item->post_type }} named {{ $post_item->post_title }} created on
          <time datetime="2016-1-1">&nbsp;{{ Carbon\Carbon::parse($post_item->post_date)->format('d-m-Y') }}</time>
        </p>
        <a href="#" class="card-header-icon" aria-label="more options">
          <span class="icon">
            <i class="fas fa-angle-down" aria-hidden="true"></i>
          </span>
        </a>
      </header>
      <div class="card-content">
        <div class="content">
          {!! $post_item->post_content !!}
        </div>
        <h3>Meta Data Linked to Post</h3>
        @foreach ($postmeta as $postmeta_item)
          @if ($postmeta_item->meta_key == 'wpcf-image' && !empty($postmeta_item->meta_value)) 
            <h4>Image</h4>
            <img width="300" src="{{ $postmeta_item->meta_value }}" />
          @endif
          @if (
            (
              $postmeta_item->meta_key == '_wp_attached_file' ||
              $postmeta_item->meta_key == '_wp_attachment_metadata'
            )
            && !empty($postmeta_item->meta_value)
          )
            <h4>Attached File</h4>
            <input class="input" value="{{ $postmeta_item->meta_value }}" />
          @endif
        @endforeach
      </div>
      <footer class="card-footer">
        <a href="/admin/import-wordpress-post/{{ $post_item->ID }}" class="card-footer-item">Import</a>
        <a href="#" class="card-footer-item">Edit</a>
      </footer>
    </div>
  </section>
  @endforeach

  <section class="section">
    <div class="container">
      <h1 class="title">Block Builder</h1>
      <h2 class="subtitle">
        A form builder that can be used to collect <strong>user input</strong> from your site visitors
      </h2>
    </div>
  </section>
</div>
@endsection
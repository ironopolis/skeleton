@extends('skeleton::layouts.admin')

@section('content')
<div class="container">
  <section class="section">
    <div class="container">
      @if (session()->has('message'))
      <div class="notification is-primary">
        {{ session('message') }}
      </div>
      @endif
      <nav class="breadcrumb" aria-label="breadcrumbs">
        <ul>
          <li><a href="/admin">Dashboard</a></li>
          <li><a href="/admin/blocks">Blocks</a></li>
          <li class="is-active"><a href="#" aria-current="page">Block</a></li>
        </ul>
      </nav>
      <h1 class="title">Block</h1>
      <h2 class="subtitle">
        Add content to your new block. Then you need to decide if this block is to be a page in its own right (that can be accessed via the slug you specify) or simply a standalone block that will be displayed within an existing page.
      </h2>
      <form method="POST" action="/admin/block/store" enctype="multipart/form-data">
        @csrf
        <block-editor :blockdebug="true" :block="{{ $block or '{}' }}" :types="{{ $types or 'null' }}"></block-editor>
      </form>
    </div>
  </section>
</div>
@endsection

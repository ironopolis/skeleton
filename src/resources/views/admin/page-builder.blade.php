@extends('skeleton::layouts.admin')

@section('content')
<div id="app" class="container">
  <section class="section">
    <div class="container">
      <h1 class="title">Page Builder</h1>
      <h2 class="subtitle">
        Create <strong>your page's</strong> layout and content.
      </h2>
    </div>
    <block-builder :pagebuilderdebug="true" :data="{{ $page }}"></block-builder>
  </section>
</div>
@endsection
@extends('skeleton::layouts.admin')

@section('content')
<div id="app" class="container">
  <section class="section">
    <div class="container">
      <nav class="breadcrumb" aria-label="breadcrumbs">
        <ul>
          <li><a href="/admin">Dashboard</a></li>
          <li class="is-active"><a href="#" aria-current="page">Homepage Builder</a></li>
        </ul>
      </nav>
      <h1 class="title">Homepage Builder</h1>
      <h2 class="subtitle">
          Tailor the contents of <strong>your Homepage</strong> to appeal to your visitors.
      </h2>
    </div>
  </section>
  <homepage-builder :homepagedebug="true" :homepagedata="{{ $body }}"></homepage-builder>
</div>
@endsection

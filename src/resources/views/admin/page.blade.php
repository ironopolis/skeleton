@extends('skeleton::layouts.admin')

@section('content')
<div id="app" class="container">
  <section class="section">
    <div class="container">
      <nav class="breadcrumb" aria-label="breadcrumbs">
        <ul>
          <li><a href="/admin">Dashboard</a></li>
          <li><a href="/admin/pages">Pages</a></li>
          <li class="is-active"><a href="#" aria-current="page">Page Builder</a></li>
        </ul>
      </nav>
      <h1 class="title">Page Builder</h1>
      <h2 class="subtitle">
          Tailor the contents of <strong>your page</strong> to appeal to your visitors.
      </h2>
    </div>
  </section>
  <page-builder :pagedebug="true" :page="{{ $body }}"></page-builder>
</div>
@endsection

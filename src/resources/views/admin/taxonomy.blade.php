@extends('skeleton::layouts.admin')

@section('content')

<div class="container">
    <section class="section">
        <div class="container">
        @if (session()->has('message'))
        <div class="notification is-primary">
            {{ session('message') }}
        </div>
        @endif
        <nav class="breadcrumb" aria-label="breadcrumbs">
            <ul>
            <li><a href="/admin">Dashboard</a></li>
            <li><a href="/admin/blocks">Taxonomy</a></li>
            <li class="is-active"><a href="#" aria-current="page">Block</a></li>
            </ul>
        </nav>
        <h1 class="title">Taxonomy</h1>
        <h2 class="subtitle">
            Organise and Categorise your blocks for use in menus and hierarchies.
        </h2>
        <form method="POST" action="/admin/block/store" enctype="multipart/form-data">
            @csrf
            <taxonomy-editor :taxonomydebug="true" :values="{{ $value }}"></taxonomy-editor>
        </form>
        </div>
    </section>
</div>
@endsection

@extends('skeleton::layouts.admin')

@section('content')
<div class="container">
  <div class="columns">
    <div class="column is-2">
      <h1 class="title">Form Submissions</h1>
    </div>
  </div>
  <h2 class="subtitle">
    View your form submissions.
  </h2>
  <div class="columns">
    <div class="column is-4">
      <nav class="breadcrumb" aria-label="breadcrumbs">
        <ul>
          <li><a href="/admin">Dashboard</a></li>
          <li class="is-active"><a href="#" aria-current="page">Form Submissions</a></li>
        </ul>
      </nav>
    </div>
  </div>
  <div class="box">
    {{ $submissions->links() }}
  </div>
  @if(!empty($submissions))
  <table class="table">
    <thead>
      <tr>
        <th><abbr title="Position">ID</abbr></th>
        <th>Submission</th>
        <th>Type</th>
        <th>Date</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tfoot>
      <tr>
        <th><abbr title="Position">ID</abbr></th>
        <th>Submission</th>
        <th>Type</th>
        <th>Date</th>
        <th>Actions</th>
      </tr>
    </tfoot>
    <tbody>
      @foreach($submissions as $submission)
        <tr {!! $submission->processed == 1 ? 'style="background: aquamarine;"' : '' !!}>
          <td>{{ $submission->id }}</td>
          <th>
            @foreach(json_decode($submission->data) as $field => $value)
            <p>{{ title_case($field) }}: {{ $value }}</p>
            @endforeach
          </th>
          <td>{{ $submission->type }}</td>
          <th>{{ $submission->created_at }}</th>
          <td>
            @if ($submission->processed == 1)
              <a href="/admin/form/unread/{{ $submission->id }}">Mark as unread</a>
            @else
              <a href="/admin/form/read/{{ $submission->id }}">Mark as read</a>
            @endif
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
  @else
  <p>You currently have no form submissions</a>
  @endif
</div>
@endsection
@extends('skeleton::layouts.admin')

@section('content')
<div id="app" class="container">
  <section class="section">
    <div class="container">
      <nav class="breadcrumb" aria-label="breadcrumbs">
        <ul>
          <li><a href="/admin">Dashboard</a></li>
          <li><a href="/admin/types" aria-current="page">Types</a></li>
          <li class="is-active"><a href="#" aria-current="page">Type</a></li>
        </ul>
      </nav>
      <h1 class="title">"{{ $type }}" Builder</h1>
      <h2 class="subtitle">
          Create routes that relate to groups of blocks of {{ $type }}s for display in various layouts such as responsive grids.
      </h2>
    </div>
  </section>
  <type-builder :typedebug="true" :type="'{{ $type }}'" :page="{{ $data }}"></type-builder>
</div>
@endsection
@extends('skeleton::layouts.admin')

@section('content')
@if (Auth::check()) 
                          You are logged in
                      @else
                          You are not logged in
                      @endif
<div id="app" class="container">
  <div class="tile is-ancestor">
    <div class="tile is-vertical is-8">
      <div class="tile">
        <div class="tile is-parent is-vertical">
          <article class="tile is-child notification is-primary">
            <p class="title">Forms...</p>
            <p class="subtitle">Start creating forms to gather data from your users. </p>
            <a href="/admin/forms" class="button">Build a form</a>
            <a href="/admin/forms/submissions" class="button">View form submissions</a>
          </article>
          <article class="tile is-child notification is-warning">
            <p class="title">Settings...</p>
            <p class="subtitle">Configure the site to meet your requirements.</p>
            <a href="/admin/settings" class="button">Get started</a>
          </article>
        </div>
        <div class="tile is-parent">
          <article class="tile is-child notification is-info">
            <p class="title">Block Editor...</p>
            <p class="subtitle">Create reusable blocks that can be put to use across your site.</p>
            <a href="/admin/blocks" class="button">Get started</a>
            <a href="/admin/import-wordpress" class="button">Importer</a>
          </article>
        </div>
      </div>
      <div class="tile is-parent">
        <article class="tile is-child notification is-danger">
          <p class="title">Taxonomies...</p>
          <p class="subtitle">Create Menus and Category Structures.</p>
          <a href="/admin/taxonomy" class="button">Get started</a>
          <div class="content">
            <!-- Content -->
          </div>
        </article>
      </div>
      <div class="tile is-parent">
        <article class="tile is-child notification is-primary">
          <p class="title">Courses...</p>
          <p class="subtitle">Manage your courses</p>
          <a href="/admin/courses" class="button">Get started</a>
          <a href="/admin/courses/import" class="button">Import Courses</a>
          <a href="/admin/dates/import" class="button">Import Course Dates</a>
          <div class="content">
            <!-- Content -->
          </div>
        </article>
      </div>
    </div>
    <div class="tile is-parent is-vertical">
      <article class="tile is-child notification is-success">
        <div class="content">
          <p class="title">Homepage Builder...</p>
          <p class="subtitle">Define the content and look for the most important page of your website...your Homepage.</p>
          <div class="content">
            <a href="/admin/homepage/build" class="button">Get started</a>
          </div>
        </div>
      </article>
      <article class="tile is-child notification is-black">
        <div class="content">
          <p class="title">Page Builder...</p>
          <p class="subtitle">Define layout and aesthetics for the pages of your website.</p>
          <div class="content">
            <a href="/admin/pages" class="button">Get started</a>
          </div>
        </div>
      </article>
      <article class="tile is-child notification is-light">
        <div class="content">
          <p class="title">Type Builder...</p>
          <p class="subtitle">Define paths/ routes for groups of blocks.</p>
          <div class="content">
            <a href="/admin/types" class="button">Get started</a>
          </div>
        </div>
      </article>
    </div>
  </div>
</div>
@endsection

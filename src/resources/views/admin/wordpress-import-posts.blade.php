@extends('skeleton::layouts.admin')

@section('content')
<div id="app" class="container">

  @foreach ($posts as $post)
  <section class="section">
    <div class="card">
      <header class="card-header">
        <p class="card-header-title">
           Content for {{ $post->post_type }} named {{ $post->post_title }} created on
          <time datetime="2016-1-1">&nbsp;{{ Carbon\Carbon::parse($post->post_date)->format('d-m-Y') }}</time>
        </p>
        <a href="#" class="card-header-icon" aria-label="more options">
          <span class="icon">
            <i class="fas fa-angle-down" aria-hidden="true"></i>
          </span>
        </a>
      </header>
      <div class="card-content">
        <div class="content">
          {!! $post->post_content !!}
        </div>
      </div>
      <footer class="card-footer">
        <a href="/admin/import-wordpress-post/{{ $post->ID }}" class="card-footer-item">Import</a>
        <a href="#" class="card-footer-item">Edit</a>
      </footer>
    </div>
  </section>
  @endforeach

  <section class="section">
    <div class="container">
      <h1 class="title">Block Builder</h1>
      <h2 class="subtitle">
        A form builder that can be used to collect <strong>user input</strong> from your site visitors
      </h2>
    </div>
  </section>
</div>
@endsection
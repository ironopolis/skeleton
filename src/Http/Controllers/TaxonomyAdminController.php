<?php

namespace Ironopolis\Skeleton\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Ironopolis\Skeleton\Body;
use Ironopolis\Skeleton\Skeleton;

class TaxonomyAdminController extends Controller
{
  public function taxonomy() {
    $skeleton = Skeleton::find(1);
    return view('skeleton::admin.taxonomy', ['value' => $skeleton->value]);
  }
}

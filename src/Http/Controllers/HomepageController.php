<?php

namespace Ironopolis\Skeleton\Http\Controllers;
use App\Http\Controllers\Controller;
use Ironopolis\Skeleton\Body;
use Illuminate\Http\Request;

class HomepageController extends Controller
{
    public function index()
    {
        return view('skeleton::homepage');
    }

    public function show()
    {
        $body = Body::select('data')->first();
        return view('skeleton::admin.homepage-builder', ['body' => $body]);
    }

    public function store(Request $request)
    {
        $body = Body::find(1);
        $body->data = json_encode($request->all());
        if ($body->save()) {
            return response()->json(['response'=>'Data was successfully added']);
        }
        return response()->json(['response'=>'There was a problem storing your selection']);
    }
}

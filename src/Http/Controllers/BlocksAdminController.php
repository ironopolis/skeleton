<?php

namespace Ironopolis\Skeleton\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Ironopolis\Skeleton\Block;
use Ironopolis\Skeleton\Body;
use DB;
use Storage;

class BlocksAdminController extends Controller
{

  public function blocks() {
    $types = Block::select('type')->groupby('type')->get();
    $blocks = Block::paginate(20);
    return view('skeleton::admin.blocks', ['blocks' => $blocks, 'types' => $types]);
  }

  public function block(Block $block) {
    $types = Block::select('type')->groupby('type')->get();
    if (Storage::disk('s3')->exists('/'.$block['image'])) {
      $block['existing_image'] = $block['image'];
      $block['image'] = Storage::disk('s3')->url($block['image']);
    }
    if (Storage::disk('s3')->exists('/'.$block['download'])) {
      $block['existing_download'] = $block['download'];
      $block['download'] = Storage::disk('s3')->url($block['download']);
    }
    return view('skeleton::admin.block', ['block' => $block, 'types' => $types]);
  }

  public function create() {
    return view('skeleton::admin.block');
  }

  public function store(Request $request) {
    $file = $request->input('existing_image');
    if ($request->exists('delete_image') && $request->input('delete_image') == "true") {
      Storage::disk('s3')->delete('/'.$file);
      $file = null;
    }
    $download = $request->input('existing_download');
    if ($request->exists('delete_download') && $request->input('delete_download') == "true") {
      Storage::disk('s3')->delete('/'.$download);
      $download = null;
    }
    if ($request->file('downloads')) {
      $downloads = request()->file('downloads');
      foreach ($downloads as $id => $download) {
        $ext = $download->guessClientExtension();
        $download_paths[] = $download->storeAs(null, $request->input('slug').'-'.$id.'.'.$ext, 's3');
      }
    }
    if ($request->file('download')) { 
      $download = request()->file('download');
      $ext = $download->guessClientExtension();
      if ($ext == 'bin') {
        $ext = $download->getClientOriginalExtension();
      }
      $download = $download->storeAs(null, $request->input('slug').'.'.$ext, 's3');
    }
    if ($request->file('image')) { 
      $file = request()->file('image');
      $ext = $file->guessClientExtension();
      $file = $file->storeAs(null, $request->input('slug').'.'.$ext, 's3');
    }
    $block = [
      'meta_title' => $request->meta_title,
      'meta_keywords' => $request->meta_keywords,
      'meta_description' => $request->meta_description,
      'slug' => $request->slug,
      'title' => $request->title,
      'subtitle' => $request->subtitle,
      'author' => $request->author,
      'image' => $file,
      'image_alt' => $request->image_alt,
      'content' => $request->content,
      'download' => $download, 
      'video_url' => $request->video_url, 
      'files' => !empty($download_paths) ? json_encode($download_paths) : '{}', 
      'type' => $request->type, 
      'published' => !empty($request->published) ? 1 : 0
    ];
    $block = Block::updateOrCreate(
      ['id' => $request->input('id')],
      $block
    );
    if (!empty($request->create_route)) {
      $page['hero']['active'] = true; 
      $page['hero']['hero'][] = [
        'title' => $request->title,
      ]; 
      $page['blocks']['active'] = true;
      $page['blocks']['blocks'][] = $block;
      Body::updateOrCreate(
      ['route' => $request->input('slug')],
      [
        'route' => $request->slug,
        'data' => json_encode($page),
        'type' => !empty($request->type) ? $request->type : 'page'
      ]);
    }
    return redirect('/admin/block/' . $block->id)
        ->with('message', 'You have created a block! Congratulations');
  }

  public function delete(Block $block) {
    $block->delete();
    return back()
      ->with('message', 'Block successfully deleted');
  }
}

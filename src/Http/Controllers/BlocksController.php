<?php

namespace Ironopolis\Skeleton\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BlocksController extends Controller
{
 
    public function import_wordpress()
    {
        $posts = DB::table('wp_posts')
                  ->select('ID', 'post_author', 'post_date', 'post_content', 'post_title', 'post_type')
                  ->where('wp_posts.post_type', 'page')
                  ->get();
       return view('skeleton::wordpress-import-posts', compact('posts'));
    }

    public function import_wordpress_post($post_id)
    {
        $post = DB::table('wp_posts')
                  ->select('ID', 'post_author', 'post_date', 'post_content', 'post_title', 'post_type')
                  ->where('wp_posts.ID', $post_id)
                  ->get()->toArray();

        $postmeta = DB::table('wp_postmeta')
                  ->where('wp_postmeta.post_id', $post_id)
                  ->get()->toArray();
        //dd($post, $postmeta, array_merge($post, $postmeta));
        return view('skeleton::wordpress-import-post', compact('post', 'postmeta'));
    }
}

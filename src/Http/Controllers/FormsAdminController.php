<?php

namespace Ironopolis\Skeleton\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Ironopolis\Skeleton\Forms;
use Ironopolis\Skeleton\FormSubmission;

class FormsAdminController extends Controller
{
  public function forms() {
    $forms = Forms::paginate(20);
    return view('skeleton::admin.forms', ['forms' => $forms]);
  }
  public function form(Forms $form) {
    return view('skeleton::admin.form', ['form' => $form->value]);
  }
  public function create() {
    return view('skeleton::admin.form');
  }
  public function store(Request $request) {
    $form = $request->input();
    $form = Forms::updateOrCreate(
      ['key' => $form[0]['key']],
      [
        'key' => $form[0]['key'],
        'value' => json_encode($form)
      ]
    );
    return response()->json(['response'=>'Data was successfully added']);
  }
  public function search(Request $request) {
    $query = Forms::where('key', 'like', '%' . $request->query('query') . '%');
    $results['results'] = $query->get();
    return json_encode($results);
  }
  public function submissions() {
    $forms_submissions = FormSubmission::orderBy('created_at', 'desc')->paginate(5);
    return view('skeleton::admin.forms-submissions', ['submissions' => $forms_submissions]);
  }
  public function unread(FormSubmission $submission) {
    $submission->processed = false;
    $submission->save();
    return back();
  }
  public function read(FormSubmission $submission) {
    $submission->processed = true;
    $submission->save();
    return back();
  }
}

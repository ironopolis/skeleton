<?php

namespace Ironopolis\Skeleton\Http\Controllers;
use App\Http\Controllers\Controller;
use Ironopolis\Skeleton\Block;
use Ironopolis\Skeleton\Body;
use Illuminate\Http\Request;

class TypesAdminController extends Controller
{
    public function types()
    {
        $types = Block::select('type')->groupby('type')->get();
        return view('skeleton::admin.types', compact('types'));
    }

    public function show($type)
    {
        $body = Body::where('type', str_plural($type))->first();
        return view('skeleton::admin.type-builder', ['type' => $type, 'data' => json_encode($body)]);
    }

    public function store(Request $request)
    {
        $content = $request->input('type'); 
        $temp['type'] = [
            'type' => $content['type'],
            'route' => $content['route'],
            'all' => $content['all']
        ];
        if ($content['all'] == true) {
            $blocks = Block::where('type', $content['type'])->orderBy('created_at', 'desc')->get();
            //dd($blocks);
            $temp['type']['blocks'] = null;
            if (!empty($blocks)) {
                foreach ($blocks as $block) {
                    $temp['type']['blocks'][] = $block;
                }
            }
        }
        $temp['hero'] = $request->input('hero');
        $blocks = json_encode($temp);
        $body = Body::updateOrCreate(
            ['type' => str_plural($content['type'])],
            [
                'route' => $content['route'],
                'data' => $blocks,
                'type' => str_plural($content['type'])
            ]
        );
        if ($body) {
            return response()->json(['response'=>'Data was successfully added']);
        }
        return response()->json(['response'=>'There was a problem storing your selection']);
    }
}

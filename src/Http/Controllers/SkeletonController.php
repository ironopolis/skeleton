<?php

namespace Ironopolis\Skeleton\Http\Controllers;

use App\Http\Controllers\Controller;
use Ironopolis\Skeleton\Skeleton;
use Ironopolis\Skeleton\Block;
use Illuminate\Http\Request;
use Storage;
use Ironopolis\Skeleton\FormSubmission;
use Ironopolis\Skeleton\Mail\Enquiry;
use Ironopolis\Skeleton\Mail\Submission;
use Mail;

class SkeletonController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {

    }

    public function index()
    {
        return view('skeleton::admin.dashboard');
    }

    public function settings()
    {
        $settings = Skeleton::select('value')->where('key', 'skeleton')->first();
        return view('skeleton::admin.settings', ['settings' => $settings->value]);
    }   

    /**
     * Store a newly created resource in storage or update existing.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Skeleton::updateOrCreate(
            ['key' => 'skeleton'],
            ['key' => 'skeleton','value' => collect($request->input())->except('_token')->toJson()]
        );
        return redirect('/admin')
            ->with('flash', 'Your settings have been applied');
    }

    public function save(Request $request)
    {
        $skeleton = Skeleton::find(1);
        $skeleton->value = json_encode($request->all());
        if ($skeleton->save()) {
            return response()->json(['response'=>'Data was successfully added']);
        }
        return response()->json(['response'=>'There was a problem storing your selection']);
    }

    public function contact() {
        $data['hero']['active'] = true;
        $data['hero']['hero'] = null;
        $data['hero']['hero'][0]['title'] = 'Contact';
        return view('skeleton::contact', ['data' => json_encode($data)]);
    }
    public function contactrequest(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required'
        ]);
        $email = new Enquiry(
            [
                'company' => $request->input('company'),
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'phone' => $request->input('phone'),
                'question' => $request->input('question')
            ]
        );
        Mail::to('info@elecsafety.co.uk')->send($email);
        return back()
            ->with('message', 'Your enquiry has been sent! We\'ll be in touch');
    }
    public function submission(Request $request) {
        $fields = $request->except('_token');
        foreach ($fields as $field => $value) {
            $content[$field] = $value;
        }
        $email = new Submission(
            $content
        );
        Mail::to('info@elecsafety.co.uk')->send($email);
        $submission = FormSubmission::create(
            [
                'type' => $request->input('type'),
                'data' => json_encode($request->except(['_token', 'type']))
            ]
        );
        return redirect('/submission-success');
    }
}

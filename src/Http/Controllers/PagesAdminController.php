<?php

namespace Ironopolis\Skeleton\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Ironopolis\Skeleton\Block;
use Ironopolis\Course\Course;
use Ironopolis\Skeleton\Body;
use DB;
use Storage;

class PagesAdminController extends Controller
{
  public function pages(Request $request) {
    $page = '1';
    $type = null;
    $search = '';
    if (!empty(request()->getQueryString())) {
      $page = request()->input('page');
    }
    if (request()->has('type')) {
      $type = request()->input('type');
    }
    $types = Block::select('type')->groupby('type')->get();
    $pages = Body::where('route', '<>', '/');
    if (request()->has('search')) {
      $search = request()->input('search');
      $pages = $pages->where('route', 'like', '%' . $search . '%');
    }
    if (in_array($type, $types->pluck('type')->toArray())) {
      $pages = $pages->where('type', $type);
    }
    $pages = $pages->paginate(10);
    return view('skeleton::admin.pages', ['pages' => $pages, 'types' => $types, 'page' => $page, 'current_type' => $type, 'search' => $search]);
  }
  public function page(Body $body) {
    return view('skeleton::admin.page', compact('body'));
  }
  public function store(Request $request) {
    if (!empty($request->page['id'])) {
      $body = Body::find($request->page['id']);
      $body->data = json_encode($request->all());
      if ($body->save()) {
          return response()->json(['response'=>'Data was successfully added']);
      }
      return response()->json(['response'=>'There was a problem storing your selection']);
    }
  }
  public function search(Request $request) {
      $query = Block::where('title', 'like', '%' . $request->query('query') . '%');
      if ($request->query('type')) {
        $query->where('type', '=', $request->query('type'));
      }
      $results['results'] = $query->get();
      return json_encode($results);
  }
  //Construct pages
  public function build(Body $body) {
    dd($body);
    return view('skeleton::admin.page-builder');
    //pass in slug as param
    $blocks = Block::where([
      ['locked', '=', 0],
      ['id', '=', $id]
    ])->get();
    dd($blocks);
    foreach($blocks as $block) {
      $json = json_encode(['page' => $block]);
      Body::create([
        'route' => $block->slug,
        'data' => $json,
        'type' => 'page'
      ]);
    }
    dd('body created');
  }
  //import pages into Bodies
  public function buildAll() {
    //pass in slug as param
    $blocks = Block::where([
      ['locked', '=', 0],
      ['id', '=', $id]
    ])->get();
    dd($blocks);
    foreach($blocks as $block) {
      $json = json_encode(['page' => $block]);
      Body::create([
        'route' => $block->slug,
        'data' => $json,
        'type' => 'page'
      ]);
    }
    dd('body created');
  }
  public function import_pages() {
    $posts = DB::table('wp_posts')
                ->select('ID', 'post_author', 'post_date', 'post_content', 'post_title', 'post_type', 'post_status')
                ->where('wp_posts.post_type', 'page')
                ->paginate(5);
    return view('skeleton::admin.import-pages', compact('posts'));
  }
  public function import_page($id, Request $request) {
    $post = DB::table('wp_posts')
                ->select('ID', 'post_author', 'post_date', 'post_content', 'post_title', 'post_type', 'post_status', 'post_name')
                ->where('wp_posts.ID', $id)
                ->first();

    $post_meta = DB::table('wp_postmeta')
                ->select('meta_key', 'meta_value')
                ->where('post_id' , '=' , $id)
                ->whereIn('meta_key', ['_thumbnail_id', 'wpcf-image'])
                ->get();
    $counter = 0;
    $post->images = [];
    $post->thumbnail = null;
    foreach ($post_meta as $meta) {
      if ($meta->meta_key == '_thumbnail_id') {
        $thumbnail_path = DB::table('wp_postmeta')
                ->select('meta_key', 'meta_value')
                ->where([
                  ['post_id' , '=' , $meta->meta_value],
                  ['meta_key' , '=' , '_wp_attached_file']
                ])
                ->first();
        if (!empty($thumbnail_path->meta_value)) {
          $post->thumbnail = 'https://www.elecsafety.co.uk/wp-content/uploads/'.$thumbnail_path->meta_value;
        } 
      }
      if ($meta->meta_key == 'wpcf-image') {
        $post->images[] = $meta->meta_value;
      }
    }
    if (!empty($request->input('import'))) {
      Block::create([
        'slug' => $post->post_name,
        'title' => $post->post_title,
        'author' => $post->post_author,
        'image' => !empty($post->thumbnail) ? $post->thumbnail : null,
        'content' => $post->post_content,
        'media' => !empty($post->images) ? json_encode($post->images) : '{}', 
        'files' => !empty($post->downloads) ? json_encode($post->downloads) : '{}', 
        'type' => $post->post_type, 
        'published' => $post->post_status == 'publish' ? 1 : 0
      ]);
    }     
    return view('skeleton::admin.import-page', compact('post'));
  }
  public function import_all($type) {
    $posts = DB::table('wp_posts')
                ->select('ID', 'post_author', 'post_date', 'post_content', 'post_title', 'post_type', 'post_status', 'post_name')
                ->where('post_type', $type)
                ->get();
    if (!empty($posts)) {
      foreach($posts as $post) {
        $page = null;
        Block::create([
          'slug' => $post->post_name,
          'title' => $post->post_title,
          'author' => $post->post_author,
          'image' => !empty($post->thumbnail) ? $post->thumbnail : null,
          'content' => $post->post_content,
          'media' => !empty($post->images) ? json_encode($post->images) : '{}', 
          'files' => !empty($post->downloads) ? json_encode($post->downloads) : '{}', 
          'type' => $post->post_type, 
          'published' => $post->post_status == 'publish' ? 1 : 0
        ]);
        $page['blocks']['blocks'][] = [
          'slug' => $post->post_name,
          'title' => $post->post_title,
          'author' => $post->post_author,
          'image' => !empty($post->thumbnail) ? $post->thumbnail : null,
          'content' => $post->post_content,
          'media' => !empty($post->images) ? json_encode($post->images) : '{}', 
          'files' => !empty($post->downloads) ? json_encode($post->downloads) : '{}', 
          'type' => $post->post_type, 
          'published' => $post->post_status == 'publish' ? 1 : 0
        ];
        $page['hero']['active'] = true;
        $page['hero']['hero'][] = [
          'title' => $post->post_title,
        ];    
        Body::create([
          'route' => $post->post_name,
          'data' => json_encode($page),
          'type' => $post->post_type
        ]);

      }
    }     
    dd('All '.$type.' imported');
    return view('skeleton::admin.import-page', compact('post'));
  }
  public function lock(Body $body) {
    $body->locked = true;
    $body->save();
    return back();
  }
  public function unlock(Body $body) {
    $body->locked = false;
    $body->save();
    return back();
  }
  public function delete(Body $body) {
    $body->delete();
    return back();
  }
  public function update(Body $body) {
    $data = json_decode($body->data);
    foreach ($data as $type => $blocks) {
      if ($type == 'blocks') {
        foreach ($blocks->blocks as $id => $block) {
          $block_update = Block::where('slug', $block->slug)->first();
          if (!empty($block_update)) {
            $data->{$type}->blocks[$id] = $block_update->toArray();
          }
        }
      } 
    }
    $body->data = json_encode($data);
    $body->save();
    return back();
  }
  public function sitemap() {
    //$base_url = env('APP_URL').'/';
    $base_url = 'https://www.elecsafety.co.uk/';
    $bodies = Body::all();
    $sitemap_start = '<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
    $sitemap_end = '</urlset>';
    $date = date('Y-m-d');
    foreach ($bodies as $body) {
      $sitemap_start = $sitemap_start . "
      <url>
        <loc>{$base_url}{$body->route}</loc>
        <lastmod>{$date}</lastmod>
      </url>
      ";
    }
    $sitemap_start = $sitemap_start . $sitemap_end;
    dd($sitemap_start);
  }
}

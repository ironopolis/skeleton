<?php

namespace Ironopolis\Skeleton;

use Illuminate\Database\Eloquent\Model;

class Skeleton extends Model
{
  protected $fillable = [
    'key', 'value'
  ];
}

<?php

namespace Ironopolis\Skeleton;

use Illuminate\Database\Eloquent\Model;

class FormSubmission extends Model
{
    protected $fillable = [
        'data', 'type'
    ];
}

<?php

namespace Ironopolis\Skeleton\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Enquiry extends Mailable
{
    use Queueable, SerializesModels;

    public $company;
    public $name;
    public $email;
    public $phone;
    public $question;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($fields)
    {
        $this->company = $fields['company'];
        $this->name = $fields['name'];
        $this->email = $fields['email'];
        $this->phone = $fields['phone'];
        $this->question = $fields['question'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('skeleton::emails.enquiry');
    }
}

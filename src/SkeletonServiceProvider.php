<?php

namespace Ironopolis\Skeleton;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;
use DB;
use Request;
use Ironopolis\Skeleton\Body;
use Illuminate\Support\Facades\Route;
use Ironopolis\Skeleton\Helpers\Meta;
use Exception;
use Storage;

class SkeletonServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        if (!strstr(Request::path(), 'admin')) {
            \View::composer('*', function ($view) {
                $menu = Storage::disk('local')->get('menu.txt');
                $view->with('menu', $menu);
                $skeleton = Skeleton::select('value')->where('key', 'skeleton')->first();
                if (!empty($skeleton)) {
                    $converted = json_decode($skeleton->value);
                    $title = !empty($converted->company_name) ? $converted->company_name : 'Please enter a company name in the settings screen' ;
                    $view->with('skeleton', $converted);
                }
                $data = DB::table('bodies')->select('data')->where('route', Request::path())->first();
                if (empty($data)) {
                    $data = DB::table('bodies')->select('data')->where('route', 'page-not-found')->first();
                }
                if (!empty($data->data)) {
                    $decoded = json_decode($data->data);
                    if (!empty($decoded->blocks) && $decoded->blocks->blocks[0]) {
                        if (empty($decoded->blocks->blocks[0]->meta_keywords)) {
                            $keywords = Meta::generateKeywords(substr($decoded->blocks->blocks[0]->content,0,1000));
                        } else {
                            $keywords = $decoded->blocks->blocks[0]->meta_keywords;
                        }
                        if (empty($decoded->blocks->blocks[0]->meta_description) && !empty($decoded->blocks->blocks[0]->content)) {
                            $description = Meta::generateDescription( $decoded->blocks->blocks[0]->content, 150, '');
                        } else {
                            $description = $decoded->blocks->blocks[0]->meta_description;
                        }
                        if (!empty($decoded->blocks->blocks[0]->meta_title)) {
                            $title = $decoded->blocks->blocks[0]->meta_title;
                        }
                        if (!empty($converted->seofriendly) && !empty($decoded->blocks->blocks[0]->content)) {
                            $view->with('content', $decoded->blocks->blocks[0]->content);
                        }
                        $view->with('keywords', $keywords);
                        $view->with('description', $description);         
                    }
                    $view->with('title', $title);
                    $view->with('data', !empty($data->data) ? $data->data : '{}');
                }
            });
        }
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'skeleton');
        $this->loadRoutesFrom(__DIR__.'/routes/web.php');
        $this->publishes([
            __DIR__.'/resources/assets' => base_path('resources/assets/packages/skeleton/'),
        ], 'public');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
